package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.BaseGameUtils;

public class GlobalScoreActivity extends Activity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener{

	FrameLayout frameAchievement,frameAllScore;
	TextView txtAchievement,txtAllScore;
	
	MediaPlayer playerMove;
	boolean soundStatus,stoppable,startable;
	
	Typeface customFont;

	private GoogleApiClient mGoogleApiClient=null;
	private static int RC_SIGN_IN = 9001;
	private boolean mResolvingConnectionFailure = false;
	private boolean mAutoStartSignInflow = true;
	private boolean mSignInClicked = false;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.global_scores);
		frameAchievement=(FrameLayout)findViewById(R.id.frameAchievement);
		frameAllScore=(FrameLayout)findViewById(R.id.frameAllScore);
		txtAchievement=(TextView)findViewById(R.id.txtAchievement);
		txtAllScore=(TextView)findViewById(R.id.txtAllScore);

		soundStatus=getIntent().getExtras().getBoolean("soundStatus");
		
		customFont=Typeface.createFromAsset(this.getAssets(), "font/isocpeur.ttf");
		txtAchievement.setTypeface(customFont, Typeface.BOLD);
		txtAllScore.setTypeface(customFont, Typeface.BOLD);

		txtAchievement.setText("Achievements");
		txtAllScore.setText("All Scores");

		startable=getIntent().getExtras().getBoolean("startable");
		stoppable=true;

		
		playerMove=MediaPlayer.create(this, R.raw.move);


	//	TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
	//	String uid = tManager.getDeviceId();

		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.build();
		mAdView.loadAd(adRequest);


		// Create the Google Api Client with access to the Play Games services
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(Games.API).addScope(Games.SCOPE_GAMES)
				.build();
		
		
		frameAchievement.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (soundStatus) playerMove.start();
				if(mGoogleApiClient!=null&&mGoogleApiClient.isConnected())
				{
					stoppable=false;
					startActivityForResult(Games.Achievements.getAchievementsIntent(mGoogleApiClient), 1);
				}
				else
				{
					if(mGoogleApiClient!=null)
					{
						mGoogleApiClient.connect();
					}
				}
			}
		});
		
		frameAllScore.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(soundStatus) playerMove.start();
				if(mGoogleApiClient!=null&&mGoogleApiClient.isConnected())
				{
					showGameOptions();
				}
				else
				{
					if(mGoogleApiClient!=null)
					{
						mGoogleApiClient.connect();
					}
				}
			}
		});

		
		
	}




	@Override
	protected void onStart() {
		super.onStart();
		if(mGoogleApiClient!=null)
		{
			mGoogleApiClient.connect();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if(mGoogleApiClient!=null)
		{
			mGoogleApiClient.disconnect();
		}
	}



	@Override
	protected void onResume() {
		super.onResume();
		if(startable&&soundStatus)
		{
			MusicClass.startMusic();
			stoppable=true;
			startable=false;
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(stoppable&&soundStatus)
		{
			MusicClass.stopMusic();
			startable=true;
			stoppable=false;
		}
	}


	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent in=new Intent(GlobalScoreActivity.this,OptionActivity.class);
		in.putExtra("soundStatus", soundStatus);
		in.putExtra("startable", false);
		stoppable=false;
		startActivity(in);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
		finish();
	}


	@Override
	public void onConnected(Bundle bundle) {

	}

	@Override
	public void onConnectionSuspended(int i) {
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		if (mResolvingConnectionFailure) {
			// already resolving
			return;
		}

		// if the sign-in button was clicked or if auto sign-in is enabled,
		// launch the sign-in flow
		if (mSignInClicked || mAutoStartSignInflow) {
			mAutoStartSignInflow = false;
			mSignInClicked = false;
			mResolvingConnectionFailure = true;

			// Attempt to resolve the connection failure using BaseGameUtils.
			// The R.string.signin_other_error value should reference a generic
			// error string in your strings.xml file, such as "There was
			// an issue with sign-in, please try again later."
			if (!BaseGameUtils.resolveConnectionFailure(this, mGoogleApiClient, connectionResult, RC_SIGN_IN, String.valueOf(1))) {
				mResolvingConnectionFailure = false;
			}
		}
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RC_SIGN_IN) {
			mSignInClicked = false;
			mResolvingConnectionFailure = false;
			if (resultCode == RESULT_OK) {
				mGoogleApiClient.connect();
			} else {
				// Bring up an error dialog to alert the user that sign-in
				// failed. The R.string.signin_failure should reference an error
				// string in your strings.xml file that tells the user they
				// could not be signed in, such as "Unable to sign in."
				BaseGameUtils.showActivityResultError(this,
						requestCode, resultCode, R.string.signin_failure);
			}
		}

	}




	public void showGameOptions()
	{

		LayoutInflater li = LayoutInflater.from(GlobalScoreActivity.this);
		View promptsView = li.inflate(R.layout.score_type, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(GlobalScoreActivity.this);
	//	builder.setView(promptsView);
		builder.setCancelable(true);
		final AlertDialog alertDialog = builder.create();
		alertDialog.setView(promptsView,0,0,0,0);
		alertDialog.show();

		LinearLayout linearGlobal=(LinearLayout)promptsView.findViewById(R.id.linearGlobal);
		LinearLayout linearLocal=(LinearLayout)promptsView.findViewById(R.id.linearLocal);
		TextView txtTitle=(TextView)promptsView.findViewById(R.id.txtTitle);
		TextView txtGlobal=(TextView)promptsView.findViewById(R.id.txtGlobal);
		TextView txtLocal=(TextView)promptsView.findViewById(R.id.txtLocal);

		txtTitle.setTypeface(customFont,Typeface.BOLD);
		txtGlobal.setTypeface(customFont,Typeface.BOLD);
		txtLocal.setTypeface(customFont,Typeface.BOLD);

		txtTitle.setText("Game Type");
		txtLocal.setText("T20");
		txtGlobal.setText("ODI");


		linearLocal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (soundStatus) {
					playerMove = MediaPlayer.create(GlobalScoreActivity.this, R.raw.move);
					playerMove.start();
				}
				alertDialog.cancel();
				stoppable=false;
				startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClient, getString(R.string.leaderboard_t20_score)), 2);
			}
		});

		linearGlobal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(soundStatus)
				{
					playerMove=MediaPlayer.create(GlobalScoreActivity.this, R.raw.move);
					playerMove.start();
				}
				alertDialog.cancel();
				stoppable=false;
				startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClient, getString(R.string.leaderboard_odi_score)), 3);
			}
		});
	}




}
