package com.w3dreamers.quizzycricket;

import android.content.Context;
import android.media.MediaPlayer;

public class MusicClass {
	
	public static Context context;
	public static MediaPlayer player;
	
	public MusicClass(Context context)
	{
		this.context=context;
		player=MediaPlayer.create(context, R.raw.music);
		player.setLooping(true);
	}
	
	public static void startMusic()
	{
		player.start();
	}
	
	public static void stopMusic()
	{
		player.pause();
	}

}
