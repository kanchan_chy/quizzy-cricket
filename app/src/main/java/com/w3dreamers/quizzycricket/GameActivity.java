package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.BaseGameUtils;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/*
import com.easyandroidanimations.library.Animation;
import com.easyandroidanimations.library.AnimationListener;
import com.easyandroidanimations.library.SlideInAnimation;
import com.easyandroidanimations.library.SlideOutAnimation;  */

public class GameActivity extends Activity implements Animation.AnimationListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
	
	LinearLayout correctLayout,linearPlay,linearRuns,linearStart,linearStartGame,linearNext,linearNextRoot,linearViewScore,linearViewRoot,linearHome,linearHomeRoot,linearQuesEnd,linearGameEnd,linearExit,linearOptionA,linearOptionB,linearOptionC,linearOptionD,linearDot,linearRun1,linearRun2,linearRun3,linearRun4,linearRun6;
	Button btnTime,btnOvers;
	TextView txtScore,txtQuesResult,txtGameResult,txtQues,txtOptionA,txtOptionB,txtOptionC,txtOptionD,txtRun1,txtRun2,txtRun3,txtRun4,txtRun6,txtStart,txtNextQues,txtViewScore,txtGoHome,txtDot;
	ImageView imgScore;
	FrameLayout frameScore;

	int[] flags={R.drawable.flag_afg,R.drawable.flag_austr,R.drawable.flag_bang,R.drawable.flag_canada,R.drawable.flag_england,R.drawable.flag_india,R.drawable.flag_ireland,R.drawable.flag_kenya,R.drawable.flag_neth,R.drawable.flag_newzealand,R.drawable.flag_pak,R.drawable.flag_scot,R.drawable.flag_south_africa,R.drawable.flag_srilanka,R.drawable.flag_usa,R.drawable.flag_west_indies,R.drawable.flag_zimba};
	String[] allColors={"#00CC99","#00CC99","#FFFFFF","#000000","#000000","#B82500","#000000","#0099CC","#000000","#009933","#CF2900","#000000","#CF2900","#FFFFFF","#0099CC","#00FF00","#FFFFFF"};
	int countryPos,firstTime=0;
	String format,state="";
	
	boolean clickable,isPlayingTic,soundStatus;
	
	boolean[] statusLevel1=new boolean[300];
	boolean[] statusLevel2=new boolean[300];
	boolean[] statusLevel3=new boolean[300];
	boolean[] statusLevel4=new boolean[300];
	boolean[] statusLevel5=new boolean[300];
	
	DbHelper dbOpenHelper;
	int serial,score,wicket,innings,previousScore,previousWicket,testOver;
	String correctAns,currentTime;
	int totalQues,level,availableQues1,availableQues2,availableQues3,availableQues4,availableQues5,usedQues1,usedQues2,usedQues3,usedQues4,usedQues5;
	
	TimeCounter counter;
	MediaPlayer playerAnswer,playerTime,playerEndTime,playerMove;

	Animation toLeft,fromLeft;
	
	SharedPreferences prefHigh;
	SharedPreferences.Editor prefHighEditor;
	
	Typeface customFont;

	private GoogleApiClient mGoogleApiClient=null;
	int noFours,noSixes;
	boolean bronze,superTest,superOdi,superT20;

	private static int RC_SIGN_IN = 9001;
	private boolean mResolvingConnectionFailure = false;
	private boolean mAutoStartSignInflow = true;
	private boolean mSignInClicked = false;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.game_layout);
		linearPlay=(LinearLayout)findViewById(R.id.linearPlay);
		linearRuns=(LinearLayout)findViewById(R.id.linearRuns);
		linearStart=(LinearLayout)findViewById(R.id.linearStart);
		linearQuesEnd=(LinearLayout)findViewById(R.id.linearQuesEnd);
		linearGameEnd=(LinearLayout)findViewById(R.id.linearGameEnd);
		//linearExit=(LinearLayout)findViewById(R.id.linearExit);
		linearOptionA=(LinearLayout)findViewById(R.id.linearOptionA);
		linearOptionB=(LinearLayout)findViewById(R.id.linearOptionB);
		linearOptionC=(LinearLayout)findViewById(R.id.linearOptionC);
		linearOptionD=(LinearLayout)findViewById(R.id.linearOptionD);
		linearDot=(LinearLayout)findViewById(R.id.linearDot);
		txtQuesResult=(TextView)findViewById(R.id.txtQuesResult);
		txtGameResult=(TextView)findViewById(R.id.txtGameResult);
		//txtOver=(TextView)findViewById(R.id.txtOvers);
		txtQues=(TextView)findViewById(R.id.txtQues);
		txtOptionA=(TextView)findViewById(R.id.txtOptionA);
		txtOptionB=(TextView)findViewById(R.id.txtOptionB);
		txtOptionC=(TextView)findViewById(R.id.txtOptionC);
		txtOptionD=(TextView)findViewById(R.id.txtOptionD);
		txtRun1=(TextView)findViewById(R.id.txtRun1);
		txtRun2=(TextView)findViewById(R.id.txtRun2);
		txtRun3=(TextView)findViewById(R.id.txtRun3);
		txtRun4=(TextView)findViewById(R.id.txtRun4);
		txtRun6=(TextView)findViewById(R.id.txtRun6);
		txtStart=(TextView)findViewById(R.id.txtStart);
		txtNextQues=(TextView)findViewById(R.id.txtNextQues);
		txtViewScore=(TextView)findViewById(R.id.txtViewScore);
		txtGoHome=(TextView)findViewById(R.id.txtGoHome);
		txtDot=(TextView)findViewById(R.id.txtDot);
		txtScore=(TextView)findViewById(R.id.txtScore);
		imgScore=(ImageView)findViewById(R.id.imgScore);
		frameScore=(FrameLayout)findViewById(R.id.frameScore);
		btnTime=(Button)findViewById(R.id.btnTime);
		btnOvers=(Button)findViewById(R.id.btnOvers);
		linearStartGame=(LinearLayout)findViewById(R.id.linearStartGame);
	    linearNext=(LinearLayout)findViewById(R.id.linearNext);
	    linearNextRoot=(LinearLayout)findViewById(R.id.linearNextRoot);
		linearViewScore=(LinearLayout)findViewById(R.id.linearViewScore);
		linearHome=(LinearLayout)findViewById(R.id.linearHome);
		linearViewRoot=(LinearLayout)findViewById(R.id.linearViewScoreRoot);
		linearHomeRoot=(LinearLayout)findViewById(R.id.linearHomeRoot);
		//btnYes=(Button)findViewById(R.id.btnYes);
		//btnNo=(Button)findViewById(R.id.btnNo);
	    linearRun1=(LinearLayout)findViewById(R.id.linearRun1);
	    linearRun2=(LinearLayout)findViewById(R.id.linearRun2);
	    linearRun3=(LinearLayout)findViewById(R.id.linearRun3);
	    linearRun4=(LinearLayout)findViewById(R.id.linearRun4);
	    linearRun6=(LinearLayout)findViewById(R.id.linearRun6);
		
		countryPos=getIntent().getExtras().getInt("position");
		imgScore.setImageResource(flags[countryPos]);
		txtScore.setTextColor(Color.parseColor(allColors[countryPos]));
		format=getIntent().getExtras().getString("format");
		soundStatus=getIntent().getExtras().getBoolean("soundStatus");
		
		linearPlay.setVisibility(View.INVISIBLE);
		linearRuns.setVisibility(View.INVISIBLE);
		linearQuesEnd.setVisibility(View.INVISIBLE);
		linearGameEnd.setVisibility(View.INVISIBLE);
		//linearExit.setVisibility(View.INVISIBLE);
		btnTime.setVisibility(View.INVISIBLE);
		linearDot.setVisibility(View.INVISIBLE);
		frameScore.setVisibility(View.INVISIBLE);
		btnOvers.setVisibility(View.INVISIBLE);
		
		linearStart.setVisibility(View.VISIBLE);
		
		prefHigh=getSharedPreferences("cricket_score", 0);
		prefHighEditor=prefHigh.edit();
		
		customFont=Typeface.createFromAsset(this.getAssets(), "font/isocpeur.ttf");

	//	txtQues.setTypeface(customFont);
	//	txtQuesResult.setTypeface(customFont);
	//	txtGameResult.setTypeface(customFont);
		btnOvers.setTypeface(customFont,Typeface.BOLD);
		txtOptionA.setTypeface(customFont,Typeface.BOLD);
		txtOptionB.setTypeface(customFont,Typeface.BOLD);
		txtOptionC.setTypeface(customFont,Typeface.BOLD);
		txtOptionD.setTypeface(customFont,Typeface.BOLD);
		txtRun1.setTypeface(customFont,Typeface.BOLD);
		txtRun2.setTypeface(customFont,Typeface.BOLD);
		txtRun3.setTypeface(customFont,Typeface.BOLD);
		txtRun4.setTypeface(customFont,Typeface.BOLD);
		txtRun6.setTypeface(customFont,Typeface.BOLD);
		txtStart.setTypeface(customFont,Typeface.BOLD);
		txtNextQues.setTypeface(customFont,Typeface.BOLD);
		txtViewScore.setTypeface(customFont,Typeface.BOLD);
		txtGoHome.setTypeface(customFont,Typeface.BOLD);
		txtDot.setTypeface(customFont,Typeface.BOLD);
		btnTime.setTypeface(customFont,Typeface.BOLD);
		txtScore.setTypeface(customFont,Typeface.BOLD);
		
		clickable=true;

		toLeft= AnimationUtils.loadAnimation(this, R.anim.swipe_to_left);
		fromLeft= AnimationUtils.loadAnimation(this,R.anim.swipe_from_left);

		toLeft.setAnimationListener(this);
		fromLeft.setAnimationListener(this);
		
		playerTime=MediaPlayer.create(GameActivity.this, R.raw.time);
		playerTime.setLooping(true);
		isPlayingTic=false;
		//counter=new TimeCounter(31000, 1000);

	//	TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
	//	String uid = tManager.getDeviceId();

		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.build();
		mAdView.loadAd(adRequest);
		
		try
		{
			dbOpenHelper = new DbHelper(GameActivity.this, Constants.DATABASE_NAME,1);
		}
		catch(Exception e)
		{}


		if((isConnectingInternet()||isWifiEnabled())&&isMapAvailable())
		{
			// Create the Google Api Client with access to the Play Games services
			mGoogleApiClient = new GoogleApiClient.Builder(this)
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this)
					.addApi(Games.API).addScope(Games.SCOPE_GAMES)
					.build();

		}
		
		
		linearStart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						if(playerMove!=null) playerMove.reset();
						playerMove=MediaPlayer.create(GameActivity.this, R.raw.move);
						playerMove.start();
					}
					startGame();
				}
			}
		});
		
		
		
		linearNext.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						if(playerMove!=null) playerMove.reset();
						playerMove=MediaPlayer.create(GameActivity.this, R.raw.move);
						playerMove.start();
					}
					showRunOptions(1);
				}
			}
		});
		
		
		linearHome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						if(playerMove!=null) playerMove.reset();
						playerMove=MediaPlayer.create(GameActivity.this, R.raw.move);
						playerMove.start();
					}
					if(innings<=2)
					{
						showRunOptions(2);
					}
					else
					{
						Intent in=new Intent(GameActivity.this, OptionActivity.class);
						in.putExtra("soundStatus", soundStatus);
						in.putExtra("startable", true);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
						finish();
					}
				}
			}
		});
		
	/*	
		btnYes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					Intent in=new Intent(GameActivity.this,OptionActivity.class);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
					finish();
				}
			}
		});
		
		
		btnNo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					new SlideOutAnimation(linearExit).setInterpolator(new DecelerateInterpolator())
					.setListener(new AnimationListener() {
						
						@Override
						public void onAnimationEnd(Animation arg1) {
							// TODO Auto-generated method stub
							linearExit.setVisibility(View.INVISIBLE);
							exitStatus=false;
							clickable=true;
						}
					}).animate();
				}
			}
		});    */
		
		
		linearDot.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					counter.cancel();
					if(isPlayingTic)
					{
						playerTime.pause();
						isPlayingTic=false;
					}
					if(soundStatus)
					{
						if(playerMove!=null) playerMove.reset();
						playerMove=MediaPlayer.create(GameActivity.this, R.raw.move);
						playerMove.start();
					}
					endOneQuestion(2);
				}
			}
		});
		
		
		linearViewScore.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						if(playerMove!=null) playerMove.reset();
						playerMove=MediaPlayer.create(GameActivity.this, R.raw.move);
						playerMove.start();
					}
					showGlobalScores();
				}
			}
		});	
		
	}
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(isPlayingTic)
		{
			currentTime=btnTime.getText().toString();
			counter.cancel();
			playerTime.pause();
		}
	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(isPlayingTic)
		{
			int ct=Integer.valueOf(currentTime);
			long ctMillies=ct*1000;
			counter=new TimeCounter(ctMillies, 1000);
			counter.start();
			playerTime.start();
		}
	}


	@Override
	protected void onStart() {
		super.onStart();
		if(mGoogleApiClient!=null)
		{
			//Toast.makeText(getApplicationContext(),"Connecting...",Toast.LENGTH_LONG).show();
			mGoogleApiClient.connect();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if(mGoogleApiClient!=null&&mGoogleApiClient.isConnected())
		{
			mGoogleApiClient.disconnect();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	/*	clickable=false;
		if(exitStatus)
		{
			new SlideOutAnimation(linearExit).setInterpolator(new DecelerateInterpolator())
			.setListener(new AnimationListener() {
				
				@Override
				public void onAnimationEnd(Animation arg1) {
					// TODO Auto-generated method stub
					linearExit.setVisibility(View.INVISIBLE);
					exitStatus=false;
					clickable=true;
				}
			}).animate();
		}
		else
		{
			new SlideInAnimation(linearExit).setInterpolator(new DecelerateInterpolator())
			.setListener(new AnimationListener() {
				
				@Override
				public void onAnimationEnd(Animation arg1) {
					// TODO Auto-generated method stub
					linearExit.setVisibility(View.VISIBLE);
					exitStatus=true;
					clickable=true;
				}
			}).animate();
		}   */
		
		if(clickable)
		{
			
			LayoutInflater li = LayoutInflater.from(GameActivity.this);
			View promptsView = li.inflate(R.layout.exit2, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
		//	builder.setView(promptsView);
			builder.setCancelable(true);
			final AlertDialog alertDialog = builder.create();
			alertDialog.setView(promptsView,0,0,0,0);
			alertDialog.show();
			
			LinearLayout linearDialogYes=(LinearLayout)promptsView.findViewById(R.id.linearDialogYes);
			LinearLayout linearDialogNo=(LinearLayout)promptsView.findViewById(R.id.linearDialogNo);
			TextView txtDialogBody=(TextView)promptsView.findViewById(R.id.txtDialogBody);
			TextView txtAlert=(TextView)promptsView.findViewById(R.id.txtAlert);
			TextView txtQuit=(TextView)promptsView.findViewById(R.id.txtQuit);
			TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);

			txtAlert.setTypeface(customFont,Typeface.BOLD);
			txtQuit.setTypeface(customFont,Typeface.BOLD);
			txtCancel.setTypeface(customFont,Typeface.BOLD);
			txtDialogBody.setTypeface(customFont);

			txtDialogBody.setText("Are you sure you want to quit the game?");
			txtAlert.setText("Alert");
			txtQuit.setText("Ok");
			txtCancel.setText("Cancel");
			
			linearDialogNo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
				}
			});	
			
			linearDialogYes.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
					Intent in = new Intent(GameActivity.this, OptionActivity.class);
					in.putExtra("soundStatus", soundStatus);
					in.putExtra("startable", true);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
					finish();
				}
			});
			
		}
		
		
	}





	@Override
	public void onAnimationStart(Animation animation) {

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		if(state.equals("showRunOptions"))
		{
			if(firstTime==0)
			{
				if(animation==toLeft)
				{
					linearStart.setVisibility(View.INVISIBLE);
					linearRuns.setVisibility(View.VISIBLE);
					linearRuns.startAnimation(fromLeft);
				}
				else
				{
					frameScore.setVisibility(View.VISIBLE);
					btnOvers.setVisibility(View.VISIBLE);
					clickable=true;
				}
			}
			else if(firstTime==1)
			{
				if(animation==toLeft)
				{
					linearQuesEnd.setVisibility(View.INVISIBLE);
					linearRuns.setVisibility(View.VISIBLE);
					linearRuns.startAnimation(fromLeft);
				}
				else clickable=true;
			}
			else
			{
				if(animation==toLeft)
				{
					linearGameEnd.setVisibility(View.INVISIBLE);
					linearRuns.setVisibility(View.VISIBLE);
					linearRuns.startAnimation(fromLeft);
				}
				else clickable=true;
			}
		}
		else if(state.equals("setPlayOptions"))
		{
			if(animation==toLeft)
			{
				linearRuns.setVisibility(View.INVISIBLE);
				linearPlay.setVisibility(View.VISIBLE);
				linearPlay.startAnimation(fromLeft);
			}
			else
			{
				linearDot.setVisibility(View.VISIBLE);
				counter=new TimeCounter(31000, 1000);
				counter.start();
				if(soundStatus)
				{
					playerTime.start();
					isPlayingTic=true;
				}
				btnTime.setVisibility(View.VISIBLE);
				clickable=true;
			}
		}
		else if(state.equals("endOneQuestion"))
		{
			if(animation==toLeft)
			{
				linearPlay.setVisibility(View.INVISIBLE);
				linearDot.setVisibility(View.INVISIBLE);
				btnTime.setVisibility(View.INVISIBLE);
				frameScore.setVisibility(View.INVISIBLE);
				btnOvers.setVisibility(View.INVISIBLE);

				String over="Over\n"+serial;
				//if(format.equals("test")) over+="Innings: "+innings+"\nOvers: "+serial;
				//else over+="Overs: "+serial;

				txtScore.setText("" + score + "/" + wicket);
				btnOvers.setText(over);

				if((serial==totalQues&&!format.equals("test"))||wicket==10||(format.equals("test")&&testOver==totalQues))
				{
					linearNextRoot.setVisibility(View.INVISIBLE);
					if((serial==totalQues&&!format.equals("test"))||wicket==10) innings++;
				}
				else linearNextRoot.setVisibility(View.VISIBLE);
				linearQuesEnd.setVisibility(View.VISIBLE);
				linearQuesEnd.startAnimation(fromLeft);
			}
			else
			{
				frameScore.setVisibility(View.VISIBLE);
				btnOvers.setVisibility(View.VISIBLE);
				if((serial==totalQues&&!format.equals("test"))||wicket==10||(format.equals("test")&&testOver==totalQues))
				{
					try
					{
						Thread.sleep(2000);
					}
					catch(Exception e) {}
					endInnings();
				}
				else
				{
					clickable=true;
				}
			}
		}
		else if(state.equals("endInnings"))
		{
			if(animation==toLeft)
			{
				linearQuesEnd.setVisibility(View.INVISIBLE);
				linearGameEnd.setVisibility(View.VISIBLE);
				linearGameEnd.startAnimation(fromLeft);
			}
			else clickable=true;
		}
	}

	@Override
	public void onAnimationRepeat(Animation animation) {

	}






	public boolean isMapAvailable()
	{
		int result= GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result== ConnectionResult.SUCCESS)
		{
			return true;
		}
	/*	else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			Toast.makeText(getApplicationContext(), "Google Play Service is not updated in your device", Toast.LENGTH_LONG).show();
			// showToast("আপানার ডিভাইসে গুগল প্লে সারভিস আপডেট করা নাই");
		}
		else
		{
			Toast.makeText(getApplicationContext(),"Google Play Service is not supported in your device",Toast.LENGTH_LONG).show();
			//showToast("আপানার ডিভাইসে গুগল প্লে সারভিস সাপোর্ট করে না");
		}    */
		return false;
	}


	public boolean isMapAvailable2()
	{
		int result= GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result== ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			//Toast.makeText(getApplicationContext(), "Google Play Service is not updated in your device", Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Google Play Service is not supported in your device", Toast.LENGTH_LONG).show();
		}
		return false;
	}


	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		if(connectivity!=null)
		{
			NetworkInfo info=connectivity.getActiveNetworkInfo();
			if(info!=null&&info.isConnected()) return true;
		}
		return false;
	}



	public boolean isWifiEnabled()
	{
		WifiManager wifi=(WifiManager)getSystemService(Context.WIFI_SERVICE);
		return wifi.isWifiEnabled();
	}




	@Override
	public void onConnected(Bundle bundle) {
		//Toast.makeText(getApplicationContext(),"Connected",Toast.LENGTH_LONG).show();
	}

	@Override
	public void onConnectionSuspended(int i) {
		//Toast.makeText(getApplicationContext(),"Connection Suspended",Toast.LENGTH_LONG).show();
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
	//	Toast.makeText(getApplicationContext(),"Connection Failed",Toast.LENGTH_LONG).show();
		if (mResolvingConnectionFailure) {
			// already resolving
			return;
		}

		// if the sign-in button was clicked or if auto sign-in is enabled,
		// launch the sign-in flow
		if (mSignInClicked || mAutoStartSignInflow) {
			mAutoStartSignInflow = false;
			mSignInClicked = false;
			mResolvingConnectionFailure = true;

			// Attempt to resolve the connection failure using BaseGameUtils.
			// The R.string.signin_other_error value should reference a generic
			// error string in your strings.xml file, such as "There was
			// an issue with sign-in, please try again later."
			if (!BaseGameUtils.resolveConnectionFailure(this,
					mGoogleApiClient, connectionResult,
					RC_SIGN_IN, String.valueOf(1))) {
				mResolvingConnectionFailure = false;
			}
		}

		// Put code here to display the sign-in button

	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RC_SIGN_IN) {
			mSignInClicked = false;
			mResolvingConnectionFailure = false;
			if (resultCode == RESULT_OK) {
				mGoogleApiClient.connect();
			} else {
				// Bring up an error dialog to alert the user that sign-in
				// failed. The R.string.signin_failure should reference an error
				// string in your strings.xml file that tells the user they
				// could not be signed in, such as "Unable to sign in."
				BaseGameUtils.showActivityResultError(this,
						requestCode, resultCode, R.string.signin_failure);
			}
		}

	}




	public void showGlobalScores()
	{
		if(isConnectingInternet()||isWifiEnabled())
		{
			if(isMapAvailable2())
			{
				Intent in=new Intent(GameActivity.this, GlobalScoreActivity.class);
				in.putExtra("soundStatus",soundStatus);
				in.putExtra("startable", true);
				startActivity(in);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
				finish();
			}
			else clickable=true;
		}
		else
		{
			LayoutInflater li = LayoutInflater.from(GameActivity.this);
			View promptsView = li.inflate(R.layout.exit2, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
		//	builder.setView(promptsView);
			builder.setCancelable(true);
			final AlertDialog alertDialog = builder.create();
			alertDialog.setView(promptsView,0,0,0,0);
			alertDialog.show();
			clickable=true;

			LinearLayout linearDialogYes=(LinearLayout)promptsView.findViewById(R.id.linearDialogYes);
			LinearLayout linearDialogNo=(LinearLayout)promptsView.findViewById(R.id.linearDialogNo);
			TextView txtDialogBody=(TextView)promptsView.findViewById(R.id.txtDialogBody);
			TextView txtAlert=(TextView)promptsView.findViewById(R.id.txtAlert);
			TextView txtQuit=(TextView)promptsView.findViewById(R.id.txtQuit);
			TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);

			txtAlert.setTypeface(customFont,Typeface.BOLD);
			txtQuit.setTypeface(customFont,Typeface.BOLD);
			txtCancel.setTypeface(customFont,Typeface.BOLD);
			txtDialogBody.setTypeface(customFont);


			txtDialogBody.setText("Please turn ON internet connection or enable WIFI of your device");
			txtAlert.setText("Alert");
			txtQuit.setText("Quit");
			txtCancel.setText("Cancel");

			linearDialogNo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(soundStatus)
					{
						if(playerMove!=null) playerMove.reset();
						playerMove=MediaPlayer.create(GameActivity.this, R.raw.move);
						playerMove.start();
					}
					alertDialog.cancel();
				}
			});

			linearDialogYes.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(soundStatus)
					{
						if(playerMove!=null) playerMove.reset();
						playerMove=MediaPlayer.create(GameActivity.this, R.raw.move);
						playerMove.start();
					}
					alertDialog.cancel();
					Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
					startActivity(in);
				}
			});
		}
	}


	
	
	
	public void startGame()
	{
		for(int i=0;i<=5;i++)
		{
			statusLevel1[i]=false;
			statusLevel2[i]=false;
			statusLevel3[i]=false;
			statusLevel4[i]=false;
			statusLevel5[i]=false;
		}
		availableQues1=63;
		availableQues2=65;
		availableQues3=90;
		availableQues4=122;
		availableQues5=130;
		usedQues1=0;
		usedQues2=0;
		usedQues3=0;
		usedQues4=0;
		usedQues5=0;

		for(int i=0;i<=availableQues1;i++)
			statusLevel1[i]=false;
		for(int i=0;i<=availableQues2;i++)
			statusLevel2[i]=false;
		for(int i=0;i<=availableQues3;i++)
			statusLevel3[i]=false;
		for(int i=0;i<=availableQues4;i++)
			statusLevel4[i]=false;
		for(int i=0;i<=availableQues5;i++)
			statusLevel5[i]=false;

		serial=0;
		score=0;
		wicket=0;
		testOver=0;
		previousScore=0;
		previousWicket=0;
		if(format.equals("test")) innings=1;
		else innings=2;
		if(format.equals("t20")) totalQues=20;
		else if(format.equals("odi")) totalQues=50;
		else if(format.equals("test")) totalQues=270;

		noFours=0;
		noSixes=0;
		bronze=false;
		superT20=false;
		superOdi=false;
		superTest=false;

		if(format.equals("test"))
		{
			btnOvers.setText("Over\n0");
		}

		showRunOptions(0);
	}
	
	
	
	public void showRunOptions(int firstTime)
	{
		this.firstTime=firstTime;
		state="showRunOptions";
		if(firstTime==0)
		{
			linearStart.startAnimation(toLeft);
	/*	    new SlideOutAnimation(linearStart).setInterpolator(new DecelerateInterpolator())
			.setListener(new AnimationListener() {

				@Override
				public void onAnimationEnd(Animation arg0) {
					// TODO Auto-generated method stub


					linearStart.setVisibility(View.INVISIBLE);
					new SlideInAnimation(linearRuns).setInterpolator(new DecelerateInterpolator())
							.setListener(new AnimationListener() {

								@Override
								public void onAnimationEnd(Animation arg1) {
									// TODO Auto-generated method stub
									linearRuns.setVisibility(View.VISIBLE);
									frameScore.setVisibility(View.VISIBLE);
									btnOvers.setVisibility(View.VISIBLE);
									clickable = true;
								}
							}).animate();

				}
			}).animate();    */
		}
		else if(firstTime==1)
		{
			linearQuesEnd.startAnimation(toLeft);
	/*		new SlideOutAnimation(linearQuesEnd).setInterpolator(new DecelerateInterpolator())
			.setListener(new AnimationListener() {
				
				@Override
				public void onAnimationEnd(Animation arg0) {
					// TODO Auto-generated method stub
					
					
					linearQuesEnd.setVisibility(View.INVISIBLE);
					new SlideInAnimation(linearRuns).setInterpolator(new DecelerateInterpolator())
					.setListener(new AnimationListener() {
						
						@Override
						public void onAnimationEnd(Animation arg1) {
							// TODO Auto-generated method stub
							linearRuns.setVisibility(View.VISIBLE);
							clickable=true;
						}
					}).animate();
					
				}
			}).animate();   */
		}
		else
		{
			linearGameEnd.startAnimation(toLeft);
	/*		new SlideOutAnimation(linearGameEnd).setInterpolator(new DecelerateInterpolator())
			.setListener(new AnimationListener() {
				
				@Override
				public void onAnimationEnd(Animation arg0) {
					// TODO Auto-generated method stub
					
					
					linearGameEnd.setVisibility(View.INVISIBLE);
					new SlideInAnimation(linearRuns).setInterpolator(new DecelerateInterpolator())
					.setListener(new AnimationListener() {
						
						@Override
						public void onAnimationEnd(Animation arg1) {
							// TODO Auto-generated method stub
							linearRuns.setVisibility(View.VISIBLE);
							clickable=true;
						}
					}).animate();
					
				}
			}).animate();    */
			
		}
		
		
	}
	
		
	public void runOptionSelected(View view)
	{
		if(clickable)
		{
			level=-1;
			if(view.getId()==R.id.linearRun1) level=1;
			else if(view.getId()==R.id.linearRun2) level=2;
			else if(view.getId()==R.id.linearRun3) level=3;
			else if(view.getId()==R.id.linearRun4) level=4;
			else if(view.getId()==R.id.linearRun6) level=5;
			if(level!=-1)
			{
				clickable=false;
				if(soundStatus)
				{
					if(playerMove!=null) playerMove.reset();
					playerMove=MediaPlayer.create(GameActivity.this, R.raw.move);
					playerMove.start();
				}
				setQuiz();
			}
		}
	}
	
	
	
	public void setQuiz()
	{
		if(usedQues1==availableQues1)
		{
			for(int i=0;i<=availableQues1;i++)
				statusLevel1[i]=false;
			usedQues1=0;
		}
		if(usedQues2==availableQues2)
		{
			for(int i=0;i<=availableQues2;i++)
				statusLevel2[i]=false;
			usedQues2=0;
		}
		if(usedQues3==availableQues3)
		{
			for(int i=0;i<=availableQues3;i++)
				statusLevel3[i]=false;
			usedQues3=0;
		}
		if(usedQues4==availableQues4)
		{
			for(int i=0;i<=availableQues4;i++)
				statusLevel4[i]=false;
			usedQues4=0;
		}
		if(usedQues5==availableQues5)
		{
			for(int i=0;i<=availableQues5;i++)
				statusLevel5[i]=false;
			usedQues5=0;
		}

		int quesPos=-1;
		Random random=new Random();
		while(true)
		{
			if(level==1)
			{
				int a=random.nextInt(availableQues1+1);
				if(statusLevel1[a]==false&&a>0)
				{
					quesPos=a;
					statusLevel1[a]=true;
					usedQues1++;
					break;
				}
			}
			else if(level==2)
			{
				int a=random.nextInt(availableQues2+1);
				if(statusLevel2[a]==false&&a>0)
				{
					quesPos=a;
					statusLevel2[a]=true;
					usedQues2++;
					break;
				}
			}
			else if(level==3)
			{
				int a=random.nextInt(availableQues3+1);
				if(statusLevel3[a]==false&&a>0)
				{
					quesPos=a;
					statusLevel3[a]=true;
					usedQues3++;
					break;
				}
			}
			else if(level==4)
			{
				int a=random.nextInt(availableQues4+1);
				if(statusLevel4[a]==false&&a>0)
				{
					quesPos=a;
					statusLevel4[a]=true;
					usedQues4++;
					break;
				}
			}
			else
			{
				int a=random.nextInt(availableQues5+1);
				if(statusLevel5[a]==false&&a>0)
				{
					quesPos=a;
					statusLevel5[a]=true;
					usedQues5++;
					break;
				}
			}
		}
		
		if(quesPos!=-1) retreiveQues(quesPos);
		
	}
	
	
	
	String purifyOption(String option)
	{
		boolean putable=false;
		String purifiedOption="";
		for(int i=option.length()-1;i>=0;i--)
		{
			if(option.charAt(i)!=' '&&putable==false) putable=true;
			if(putable)
			{
				purifiedOption+=option.charAt(i);
			}
		}
		option=purifiedOption;
		putable=false;
	    purifiedOption="";
	    for(int i=option.length()-1;i>=0;i--)
		{
			if(option.charAt(i)!=' '&&putable==false) putable=true;
			if(putable)
			{
				purifiedOption+=option.charAt(i);
			}
		}
		return purifiedOption;
	}
	
	
	
	public void retreiveQues(int quesPos)
	{
		String question="",optionA="",optionB="",optionC="",optionD="",answer="";
		
		try
		{
			if(level==1)
			{
				ArrayList<String>columnName=new ArrayList<String>();
				columnName.add(Constants.LEVEL_1_QUESTION);
				columnName.add(Constants.LEVEL_1_OPTION_A);
				columnName.add(Constants.LEVEL_1_OPTION_B);
				columnName.add(Constants.LEVEL_1_OPTION_C);
				columnName.add(Constants.LEVEL_1_OPTION_D);
				columnName.add(Constants.LEVEL_1_ANSWER);
				HashMap<String,ArrayList<String>> data=dbOpenHelper.getSelectedRowNumber(Constants.TABLE_LEVEL_1, columnName, Constants.LEVEL_1_ID, Constants.IS_EQUAL,String.valueOf(quesPos),Constants.LEVEL_1_ID);
				if(data.get(Constants.LEVEL_1_QUESTION).size()>0) question=data.get(Constants.LEVEL_1_QUESTION).get(0);
				if(data.get(Constants.LEVEL_1_OPTION_A).size()>0) optionA=data.get(Constants.LEVEL_1_OPTION_A).get(0);
				if(data.get(Constants.LEVEL_1_OPTION_B).size()>0) optionB=data.get(Constants.LEVEL_1_OPTION_B).get(0);
				if(data.get(Constants.LEVEL_1_OPTION_C).size()>0) optionC=data.get(Constants.LEVEL_1_OPTION_C).get(0);
				if(data.get(Constants.LEVEL_1_OPTION_D).size()>0) optionD=data.get(Constants.LEVEL_1_OPTION_D).get(0);
				if(data.get(Constants.LEVEL_1_ANSWER).size()>0) answer=data.get(Constants.LEVEL_1_ANSWER).get(0);
			}
			else if(level==2)
			{
				ArrayList<String>columnName=new ArrayList<String>();
				columnName.add(Constants.LEVEL_2_QUESTION);
				columnName.add(Constants.LEVEL_2_OPTION_A);
				columnName.add(Constants.LEVEL_2_OPTION_B);
				columnName.add(Constants.LEVEL_2_OPTION_C);
				columnName.add(Constants.LEVEL_2_OPTION_D);
				columnName.add(Constants.LEVEL_2_ANSWER);
				HashMap<String,ArrayList<String>> data=dbOpenHelper.getSelectedRowNumber(Constants.TABLE_LEVEL_2, columnName, Constants.LEVEL_2_ID, Constants.IS_EQUAL,String.valueOf(quesPos),Constants.LEVEL_2_ID);
				if(data.get(Constants.LEVEL_2_QUESTION).size()>0) question=data.get(Constants.LEVEL_2_QUESTION).get(0);
				if(data.get(Constants.LEVEL_2_OPTION_A).size()>0) optionA=data.get(Constants.LEVEL_2_OPTION_A).get(0);
				if(data.get(Constants.LEVEL_2_OPTION_B).size()>0) optionB=data.get(Constants.LEVEL_2_OPTION_B).get(0);
				if(data.get(Constants.LEVEL_2_OPTION_C).size()>0) optionC=data.get(Constants.LEVEL_2_OPTION_C).get(0);
				if(data.get(Constants.LEVEL_2_OPTION_D).size()>0) optionD=data.get(Constants.LEVEL_2_OPTION_D).get(0);
				if(data.get(Constants.LEVEL_2_ANSWER).size()>0) answer=data.get(Constants.LEVEL_2_ANSWER).get(0);
			}
			else if(level==3)
			{
				ArrayList<String>columnName=new ArrayList<String>();
				columnName.add(Constants.LEVEL_3_QUESTION);
				columnName.add(Constants.LEVEL_3_OPTION_A);
				columnName.add(Constants.LEVEL_3_OPTION_B);
				columnName.add(Constants.LEVEL_3_OPTION_C);
				columnName.add(Constants.LEVEL_3_OPTION_D);
				columnName.add(Constants.LEVEL_3_ANSWER);
				HashMap<String,ArrayList<String>> data=dbOpenHelper.getSelectedRowNumber(Constants.TABLE_LEVEL_3, columnName, Constants.LEVEL_3_ID, Constants.IS_EQUAL,String.valueOf(quesPos),Constants.LEVEL_3_ID);
				if(data.get(Constants.LEVEL_3_QUESTION).size()>0) question=data.get(Constants.LEVEL_3_QUESTION).get(0);
				if(data.get(Constants.LEVEL_3_OPTION_A).size()>0) optionA=data.get(Constants.LEVEL_3_OPTION_A).get(0);
				if(data.get(Constants.LEVEL_3_OPTION_B).size()>0) optionB=data.get(Constants.LEVEL_3_OPTION_B).get(0);
				if(data.get(Constants.LEVEL_3_OPTION_C).size()>0) optionC=data.get(Constants.LEVEL_3_OPTION_C).get(0);
				if(data.get(Constants.LEVEL_3_OPTION_D).size()>0) optionD=data.get(Constants.LEVEL_3_OPTION_D).get(0);
				if(data.get(Constants.LEVEL_3_ANSWER).size()>0) answer=data.get(Constants.LEVEL_3_ANSWER).get(0);
			}
			else if(level==4)
			{
				ArrayList<String>columnName=new ArrayList<String>();
				columnName.add(Constants.LEVEL_4_QUESTION);
				columnName.add(Constants.LEVEL_4_OPTION_A);
				columnName.add(Constants.LEVEL_4_OPTION_B);
				columnName.add(Constants.LEVEL_4_OPTION_C);
				columnName.add(Constants.LEVEL_4_OPTION_D);
				columnName.add(Constants.LEVEL_4_ANSWER);
				HashMap<String,ArrayList<String>> data=dbOpenHelper.getSelectedRowNumber(Constants.TABLE_LEVEL_4, columnName, Constants.LEVEL_4_ID, Constants.IS_EQUAL,String.valueOf(quesPos),Constants.LEVEL_4_ID);
				if(data.get(Constants.LEVEL_4_QUESTION).size()>0) question=data.get(Constants.LEVEL_4_QUESTION).get(0);
				if(data.get(Constants.LEVEL_4_OPTION_A).size()>0) optionA=data.get(Constants.LEVEL_4_OPTION_A).get(0);
				if(data.get(Constants.LEVEL_4_OPTION_B).size()>0) optionB=data.get(Constants.LEVEL_4_OPTION_B).get(0);
				if(data.get(Constants.LEVEL_4_OPTION_C).size()>0) optionC=data.get(Constants.LEVEL_4_OPTION_C).get(0);
				if(data.get(Constants.LEVEL_4_OPTION_D).size()>0) optionD=data.get(Constants.LEVEL_4_OPTION_D).get(0);
				if(data.get(Constants.LEVEL_4_ANSWER).size()>0) answer=data.get(Constants.LEVEL_4_ANSWER).get(0);
			}
			else
			{
				ArrayList<String>columnName=new ArrayList<String>();
				columnName.add(Constants.LEVEL_5_QUESTION);
				columnName.add(Constants.LEVEL_5_OPTION_A);
				columnName.add(Constants.LEVEL_5_OPTION_B);
				columnName.add(Constants.LEVEL_5_OPTION_C);
				columnName.add(Constants.LEVEL_5_OPTION_D);
				columnName.add(Constants.LEVEL_5_ANSWER);
				HashMap<String,ArrayList<String>> data=dbOpenHelper.getSelectedRowNumber(Constants.TABLE_LEVEL_5, columnName, Constants.LEVEL_5_ID, Constants.IS_EQUAL,String.valueOf(quesPos),Constants.LEVEL_5_ID);
				if(data.get(Constants.LEVEL_5_QUESTION).size()>0) question=data.get(Constants.LEVEL_5_QUESTION).get(0);
				if(data.get(Constants.LEVEL_5_OPTION_A).size()>0) optionA=data.get(Constants.LEVEL_5_OPTION_A).get(0);
				if(data.get(Constants.LEVEL_5_OPTION_B).size()>0) optionB=data.get(Constants.LEVEL_5_OPTION_B).get(0);
				if(data.get(Constants.LEVEL_5_OPTION_C).size()>0) optionC=data.get(Constants.LEVEL_5_OPTION_C).get(0);
				if(data.get(Constants.LEVEL_5_OPTION_D).size()>0) optionD=data.get(Constants.LEVEL_5_OPTION_D).get(0);
				if(data.get(Constants.LEVEL_5_ANSWER).size()>0) answer=data.get(Constants.LEVEL_5_ANSWER).get(0);
			}
			question=purifyOption(question);
			optionA=purifyOption(optionA);
			optionB=purifyOption(optionB);
			optionC=purifyOption(optionC);
			optionD=purifyOption(optionD);
			answer=purifyOption(answer);
			correctAns=answer;
			setPlayOptions(question, optionA, optionB, optionC, optionD, answer);
			
		}
		catch(Exception e) {}
	}
	
	
	
	
	public void setPlayOptions(String question,String optionA,String optionB,String optionC,String optionD,String answer)
	{
		txtQues.setText(question);
		txtOptionA.setText(optionA);
		txtOptionB.setText(optionB);
		txtOptionC.setText(optionC);
		txtOptionD.setText(optionD);
		
		linearOptionA.setBackgroundResource(R.drawable.background_button);
		linearOptionB.setBackgroundResource(R.drawable.background_button);
		linearOptionC.setBackgroundResource(R.drawable.background_button);
		linearOptionD.setBackgroundResource(R.drawable.background_button);

		state="setPlayOptions";
		linearRuns.startAnimation(toLeft);
		
	/*    new SlideOutAnimation(linearRuns).setInterpolator(new DecelerateInterpolator())
		.setListener(new AnimationListener() {
			
			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub
				linearRuns.setVisibility(View.INVISIBLE);
				new SlideInAnimation(linearPlay).setInterpolator(new DecelerateInterpolator())
				.setListener(new AnimationListener() {
					
					@Override
					public void onAnimationEnd(Animation arg1) {
						// TODO Auto-generated method stub
						linearPlay.setVisibility(View.VISIBLE);
						linearDot.setVisibility(View.VISIBLE);
						counter=new TimeCounter(31000, 1000);
						counter.start();
						if(soundStatus)
						{
							playerTime.start();
							isPlayingTic=true;
						}
						btnTime.setVisibility(View.VISIBLE);
						clickable=true;
					}
				}).animate();
				
			}
		}).animate();    */
		
	}
	
	
	
	
	public void playOptionSelected(View view)
	{
		if(clickable)
		{
			clickable=false;
			if(view.getId()==R.id.linearOptionA)
			{
				if((txtOptionA.getText().toString()).equals(correctAns))
				{
					showEachResult(true,R.id.linearOptionA);
				}
				else
				{
					showEachResult(false,R.id.linearOptionA);
				}
			}
			else if(view.getId()==R.id.linearOptionB)
			{
				if((txtOptionB.getText().toString()).equals(correctAns))
				{
					showEachResult(true,R.id.linearOptionB);
				}
				else
				{
					showEachResult(false,R.id.linearOptionB);
				}
			}
			else if(view.getId()==R.id.linearOptionC)
			{
				if((txtOptionC.getText().toString()).equals(correctAns))
				{
					showEachResult(true,R.id.linearOptionC);
				}
				else
				{
					showEachResult(false,R.id.linearOptionC);
				}
			}
			else if(view.getId()==R.id.linearOptionD)
			{
				if((txtOptionD.getText().toString()).equals(correctAns))
				{
					showEachResult(true,R.id.linearOptionD);
				}
				else
				{
					showEachResult(false,R.id.linearOptionD);
				}
			}
		}
	}
	
	
	
	
	public void showEachResult(boolean isCorrect,int selectedOption)
	{
		counter.cancel();
		if(isPlayingTic)
		{
			playerTime.pause();
			isPlayingTic=false;
		}
		if(soundStatus)
		{
			if(playerAnswer!=null) playerAnswer.reset();
			if(isCorrect) playerAnswer=MediaPlayer.create(GameActivity.this, R.raw.correct_answer);
			else playerAnswer=MediaPlayer.create(GameActivity.this, R.raw.wrong_answer);
			playerAnswer.start();
		}
		LinearLayout selectedLayout;
		selectedLayout=(LinearLayout)findViewById(selectedOption);
		if(isCorrect)
		{
			if(level==5) score+=6;
			else score+=level;
			if(level==5)
			{
				noSixes=noSixes+1;
				noFours=0;
			}
			else if(level==4)
			{
				noFours=noFours+1;
				noSixes=0;
			}
			else
			{
				noFours=0;
				noSixes=0;
			}
			if(mGoogleApiClient!=null)
			{
				if(mGoogleApiClient.isConnected())
				{
					if(noFours==3||noSixes==3)
					{
						Games.Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_super_hitter));
					}
					if(noFours==6)
					{
						Games.Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_silver));
					}
					if(noSixes==6)
					{
						Games.Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_gold));
					}
					if(score>=50&&wicket==0&&bronze==false)
					{
						Games.Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_bronze));
						bronze=true;
					}
					if(format.equals("t20"))
					{
						if(score>=80&&superT20==false)
						{
							Games.Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_super_t20_player));
							superT20=true;
						}
					}
					else if(format.equals("odi"))
					{
						if(score>=125&&superOdi==false)
						{
							Games.Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_super_odi_player));
							superOdi=true;
						}
					}
					else
					{
						int value;
						if(innings==1) value=score;
						else value=previousScore+score;
						if(value>=250&&superTest==false)
						{
							Games.Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_super_test_player));
							superTest=true;
						}
					}
				}
			}
			//Toast.makeText(getApplicationContext(), "Congrates. Answer is correct", Toast.LENGTH_LONG).show();
			//selectedLayout.setBackgroundColor(Color.parseColor("#008000"));
			selectedLayout.setBackgroundResource(R.drawable.back_correct);
			CorrectTask task=new CorrectTask();
			task.execute(true);
		}
		else
		{
			wicket++;
		    correctLayout=null;
			if((txtOptionA.getText().toString()).equals(correctAns)) correctLayout=linearOptionA;
			else if((txtOptionB.getText().toString()).equals(correctAns)) correctLayout=linearOptionB;
			else if((txtOptionC.getText().toString()).equals(correctAns)) correctLayout=linearOptionC;
			else if((txtOptionD.getText().toString()).equals(correctAns)) correctLayout=linearOptionD;
			//Toast.makeText(getApplicationContext(), "You are wrong! Correct answer:\n"+correctLayout.getText().toString(),Toast.LENGTH_LONG).show();
			//selectedLayout.setBackgroundColor(Color.parseColor("#FF4500"));
			selectedLayout.setBackgroundResource(R.drawable.back_wrong);
			WrongTask task=new WrongTask();
			task.execute();
		}
	}
	
	
	public void callCorrectTask()
	{
		//correctLayout.setBackgroundColor(Color.parseColor("#008000"));
		correctLayout.setBackgroundResource(R.drawable.back_correct);
		CorrectTask task=new CorrectTask();
		task.execute(false);
	}
	
	
	
	public boolean isGreater(int highScore1,int wicket1,int highScore2,int wicket2)
	{
		if(highScore1>highScore2) return true;
		else if(highScore1==highScore2)
		{
			if(wicket1<wicket2) return true;
			else return false;
		}
		else return false;
	}
	
	
	
	public void endOneQuestion(int answerFlag)
	{
		serial++;
		if(format.equals("test")) testOver++;

		String quesResult,run,overName;
		if(answerFlag==1)
		{
			quesResult="Congrates!! ";
			if(level==1) run="You scored 1 run. ";
			else if(level==5) run="You scored 6 runs. ";
			else run="You scored "+level+" runs. ";
			quesResult+=run;
		}
		else if(answerFlag==0) quesResult="Out!! ";
		else if(answerFlag==2) quesResult="No run!! ";
		else quesResult="Out!! Time is over! ";
		
		if(serial<=1) overName="over";
		else overName="overs";
		quesResult+="Your current score is "+score+"/"+wicket+" in "+serial+" "+overName;
		if(format.equals("test")) quesResult+=" at Innings "+innings+".";
		else quesResult+=".";
		
		txtQuesResult.setText(quesResult);

		state="endOneQuestion";
		linearPlay.startAnimation(toLeft);
		
	/*    new SlideOutAnimation(linearPlay).setInterpolator(new DecelerateInterpolator())
		.setListener(new AnimationListener() {
			
			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub				
				
				linearPlay.setVisibility(View.INVISIBLE);
				linearDot.setVisibility(View.INVISIBLE);
				btnTime.setVisibility(View.INVISIBLE);
				frameScore.setVisibility(View.INVISIBLE);
				btnOvers.setVisibility(View.INVISIBLE);
				
				String over="Over\n"+serial;
				//if(format.equals("test")) over+="Innings: "+innings+"\nOvers: "+serial;
				//else over+="Overs: "+serial;
				
				txtScore.setText(""+score+"/"+wicket);
				btnOvers.setText(over);
				
				if((serial==totalQues&&!format.equals("test"))||wicket==10||(format.equals("test")&&testOver==totalQues))
				{
					linearNextRoot.setVisibility(View.INVISIBLE);
					if((serial==totalQues&&!format.equals("test"))||wicket==10) innings++;
				}
				else linearNextRoot.setVisibility(View.VISIBLE);
				
				frameScore.setVisibility(View.VISIBLE);
				btnOvers.setVisibility(View.VISIBLE);
								
				new SlideInAnimation(linearQuesEnd).setInterpolator(new DecelerateInterpolator())
				.setListener(new AnimationListener() {
					
					@Override
					public void onAnimationEnd(Animation arg1) {
						// TODO Auto-generated method stub
						linearQuesEnd.setVisibility(View.VISIBLE);
						
						if((serial==totalQues&&!format.equals("test"))||wicket==10||(format.equals("test")&&testOver==totalQues))
						{
							try
							{
								Thread.sleep(2000);
							}
							catch(Exception e) {}
							endInnings();
						}
						else clickable=true;
					}
				}).animate();
				
			}
		}).animate();    */
		
	}
	
	
	
	public void endInnings()
	{
		if(innings>2||testOver==totalQues)
		{

			
			String[] high=new String[3];
			if(format.equals("test"))
			{
				high[0]=prefHigh.getString("test_1", "-1/10_2");
				high[1]=prefHigh.getString("test_2", "-1/10_2");
			}
			else
			{
				high[0]=prefHigh.getString(format, "-1/10_2");
			}
					
			int[] highScore=new int[3];
			int[] highWicket=new int[3];
			int[] highPos=new int[3];
			
			for(int j=0;j<2;j++)
			{
				
				highScore[j]=0;
				highWicket[j]=0;
				highPos[j]=0;
				boolean minusStatus=false,wicketStatus=false,posStatus=false;
				for(int i=0;i<high[j].length();i++)
				{
					if(high[j].charAt(i)=='-')
					{
						minusStatus=true;
					}
					else if(high[j].charAt(i)=='/')
					{
						wicketStatus=true;
					}
					else if(high[j].charAt(i)=='_')
					{
						posStatus=true;
					}
					else
					{
						if(posStatus)
						{
							highPos[j]=highPos[j]*10+(high[j].charAt(i)-48);
						}
						else if(wicketStatus)
						{
							highWicket[j]=highWicket[j]*10+(high[j].charAt(i)-48);
						}
						else
						{
							if(minusStatus) highScore[j]=highScore[j]*10-(high[j].charAt(i)-48);
							else highScore[j]=highScore[j]*10+(high[j].charAt(i)-48);
						}
					}
				}
				if(j==0&&(!format.equals("test")))
				{
					break;
				}
			}

			if(!format.equals("test"))
			{
				if(isGreater(score, wicket, highScore[0], highWicket[0]))
				{
					prefHighEditor.putString(format, score+"/"+wicket+"_"+countryPos);
					prefHighEditor.commit();
				}
			}
			else
			{
				int scoreHigh=highScore[0]+highScore[1];
				int wicketHigh=highWicket[0]+highWicket[1];
				int scoreCurrent=score+previousScore;
				int wicketCurrent=wicket+previousWicket;
				if(isGreater(scoreCurrent,wicketCurrent,scoreHigh,wicketHigh))
				{
					if(innings>=2)
					{
						prefHighEditor.putString("test_1", previousScore+"/"+previousWicket+"_"+countryPos);
						prefHighEditor.putString("test_2", score+"/"+wicket+"_"+countryPos);
					}
					else
					{
						prefHighEditor.putString("test_1", score+"/"+wicket+"_"+countryPos);
						prefHighEditor.putString("test_2", previousScore+"/"+previousWicket+"_"+countryPos);
					}
					prefHighEditor.commit();
				}
			}

			if(mGoogleApiClient!=null)
			{
				if(mGoogleApiClient.isConnected())
				{
					if(format.equals("t20")) Games.Leaderboards.submitScore(mGoogleApiClient, getString(R.string.leaderboard_t20_score), score);
					if(format.equals("odi")) Games.Leaderboards.submitScore(mGoogleApiClient, getString(R.string.leaderboard_odi_score), score);
				}
			}

			
		}
		
		
		
		String gameResult="",overName;
		
		if(serial<=1) overName="over";
		else overName="overs";
		if(testOver==totalQues)
		{
			gameResult=gameResult+"Game Over!! All days completed.";
			if(innings<=1) gameResult=gameResult+" Your total score is "+score+"/"+wicket+" in "+serial+" "+overName+" at Innings 1.";
			else gameResult=gameResult+" Your total score is "+previousScore+"/"+previousWicket+" at Innings 1 and "+score+"/"+wicket+" at Innings 2.";
			linearViewRoot.setVisibility(View.VISIBLE);
			txtGoHome.setText("Go Home");
		}
		else if(innings<=2)
		{
            gameResult+="Innings Over!! Your total score is "+score+"/"+wicket+" in "+serial+" "+overName+" at Innings 1.";		
            linearViewRoot.setVisibility(View.INVISIBLE);
            txtGoHome.setText("Next Innings");
            previousScore=score;
            previousWicket=wicket;
			score=0;
			wicket=0;
			serial=0;
			txtScore.setText("0/0");
			btnOvers.setText("Over\n0");
		}
		else
		{
			if(!format.equals("test")) gameResult+="Game Over!! Your total score is "+score+"/"+wicket+" in "+serial+" "+overName+".";
			else gameResult+="Game Over!! Your total score is "+previousScore+"/"+previousWicket+" at Innings 1 and "+score+"/"+wicket+" at Innings 2.";
			linearViewRoot.setVisibility(View.VISIBLE);
            txtGoHome.setText("Go Home");
		}
		
		txtGameResult.setText(gameResult);

		state="endInnings";
		linearQuesEnd.startAnimation(toLeft);

	/*    new SlideOutAnimation(linearQuesEnd).setInterpolator(new DecelerateInterpolator())
		.setListener(new AnimationListener() {
			
			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub				
				
				linearQuesEnd.setVisibility(View.INVISIBLE);
								
				new SlideInAnimation(linearGameEnd).setInterpolator(new DecelerateInterpolator())
				.setListener(new AnimationListener() {
					
					@Override
					public void onAnimationEnd(Animation arg1) {
						// TODO Auto-generated method stub
						linearGameEnd.setVisibility(View.VISIBLE);
						clickable=true;
					}
				}).animate();
				
			}
		}).animate();    */
			
		
	}
	/*

	@Override
	public void onSignInFailed() {

	}

	@Override
	public void onSignInSucceeded() {

	}   */


	public class TimeCounter extends CountDownTimer
	{

		public TimeCounter(long timeMilies, long intervalMilies) {
			super(timeMilies, intervalMilies);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			wicket++;
			if(isPlayingTic)
			{
				playerTime.pause();
				isPlayingTic=false;
			}
			btnTime.setText("0");
			if(soundStatus)
			{
				if(playerEndTime!=null) playerEndTime.reset();
				playerEndTime=MediaPlayer.create(GameActivity.this, R.raw.endtime);
				playerEndTime.start();
			}
			endOneQuestion(3);
		}

		@Override
		public void onTick(long time) {
			// TODO Auto-generated method stub
			if((time/1000)<=30)
			{
				btnTime.setText(""+(time/1000));
			}
		}
		
	}
	
	
	
	class CorrectTask extends AsyncTask<Boolean, String, String>
	{
		boolean answerFlag;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if(answerFlag) endOneQuestion(1);
			else endOneQuestion(0);
		}

		@Override
		protected String doInBackground(Boolean... params) {
			// TODO Auto-generated method stub
			answerFlag=params[0];
			try{
				Thread.sleep(1300);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return null;
		}
		
	}
	
	
	class WrongTask extends AsyncTask<Boolean, String, String>
	{
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			callCorrectTask();
		}

		@Override
		protected String doInBackground(Boolean... params) {
			// TODO Auto-generated method stub
			try{
				Thread.sleep(1300);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return null;
		}
		
	}
	
	
	

}
