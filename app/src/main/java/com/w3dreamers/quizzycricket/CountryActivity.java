package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class CountryActivity extends Activity implements Animation.AnimationListener{

	TextView txtGo,txtCountry;
	ImageView imgLeft,imgRight,imgCountry;
	//Button btnCountry;
	LinearLayout linearCountry,linearCountryName;
	FrameLayout frameGo;
	
	String[] countries={"Afganistan","Australia","Bangladesh","Canada","England","India","Ireland","Kenea","Netherland","Newzeland","Pakistan","Scotland","South Africa","Srilanka","USA","West Indies","Zimbabuwe"};
	int[] flags={R.drawable.flag_afg,R.drawable.flag_austr,R.drawable.flag_bang,R.drawable.flag_canada,R.drawable.flag_england,R.drawable.flag_india,R.drawable.flag_ireland,R.drawable.flag_kenya,R.drawable.flag_neth,R.drawable.flag_newzealand,R.drawable.flag_pak,R.drawable.flag_scot,R.drawable.flag_south_africa,R.drawable.flag_srilanka,R.drawable.flag_usa,R.drawable.flag_west_indies,R.drawable.flag_zimba};
	int position;
	boolean clickable,soundStatus,stoppable,startable;
	MediaPlayer playerMove;
	
	Typeface customFont;
	
	float x1,x2;
    float y1, y2;
    static final int MIN_DISTANCE = 150;

	Animation inFade;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.country_layout);
		txtGo=(TextView)findViewById(R.id.txtGo);
		txtCountry=(TextView)findViewById(R.id.txtCountry);
		imgLeft=(ImageView)findViewById(R.id.imgLeft);
		imgRight=(ImageView)findViewById(R.id.imgRight);
		imgCountry=(ImageView)findViewById(R.id.imgCountry);
		//btnCountry=(Button)findViewById(R.id.btnCountry);
	    frameGo=(FrameLayout)findViewById(R.id.frameGo);
		linearCountry=(LinearLayout)findViewById(R.id.linearCountry);
		linearCountryName=(LinearLayout)findViewById(R.id.linearCountryName);
		
		position=getIntent().getExtras().getInt("position");
		soundStatus=getIntent().getExtras().getBoolean("soundStatus");
		
		customFont=Typeface.createFromAsset(this.getAssets(), "font/isocpeur.ttf");
		txtGo.setTypeface(customFont,Typeface.BOLD);
		txtCountry.setTypeface(customFont,Typeface.BOLD);
		
		imgCountry.setImageResource(flags[position]);
		txtCountry.setText(countries[position]);
		
		clickable=true;
		startable=false;
		stoppable=true;

		inFade= AnimationUtils.loadAnimation(this, R.anim.slide_in_fade);

		inFade.setAnimationListener(this);
		//outFade.setAnimationListener(this);

	//	TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
	//	String uid = tManager.getDeviceId();

		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.build();
		mAdView.loadAd(adRequest);
		
		imgLeft.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (clickable) {
					clickable = false;
					if (soundStatus) {
						playerMove = MediaPlayer.create(CountryActivity.this, R.raw.move);
						playerMove.start();
					}
					position--;
					if (position < 0) position = countries.length - 1;
					imgCountry.setImageResource(flags[position]);
					txtCountry.setText(countries[position]);
					linearCountry.startAnimation(inFade);
				}
			}
		});
		
		imgRight.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(CountryActivity.this, R.raw.move);
						playerMove.start();
					}
					position++;
					if(position>=countries.length) position=0;
					imgCountry.setImageResource(flags[position]);
					txtCountry.setText(countries[position]);
					linearCountry.startAnimation(inFade);
				}
			}
		});
		
		
		frameGo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(CountryActivity.this, R.raw.move);
						playerMove.start();
					}
					Intent in=new Intent(CountryActivity.this, CricFormatActivity.class);
					in.putExtra("position", position);
					in.putExtra("soundStatus", soundStatus);
					stoppable=false;
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();
				}
			}
		});



		linearCountryName.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(CountryActivity.this, R.raw.move);
						playerMove.start();
					}
					Intent in=new Intent(CountryActivity.this,CricFormatActivity.class);
					in.putExtra("position", position);
					in.putExtra("soundStatus", soundStatus);
					stoppable=true;
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();
				}
			}
		});

		
	}



	@Override
	protected void onResume() {
		super.onResume();
		if(startable&&soundStatus)
		{
			MusicClass.startMusic();
			stoppable=true;
			startable=false;
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(stoppable&&soundStatus)
		{
			MusicClass.stopMusic();
			startable=true;
			stoppable=false;
		}
	}


	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(clickable)
		{
			clickable=false;
			Intent in=new Intent(CountryActivity.this, OptionActivity.class);
			in.putExtra("soundStatus", soundStatus);
			in.putExtra("startable", false);
			stoppable=false;
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
			finish();
		}
	}


	@Override
	public void onAnimationStart(Animation animation) {

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		clickable=true;
	}

	@Override
	public void onAnimationRepeat(Animation animation) {

	}
}
