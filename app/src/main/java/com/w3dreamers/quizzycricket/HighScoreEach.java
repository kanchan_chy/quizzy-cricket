package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class HighScoreEach extends Activity{
	
	TextView txtScore;
	ImageView imgScore;
	FrameLayout frameScore;
	
	int[] flags={R.drawable.flag_afg,R.drawable.flag_austr,R.drawable.flag_bang,R.drawable.flag_canada,R.drawable.flag_england,R.drawable.flag_india,R.drawable.flag_ireland,R.drawable.flag_kenya,R.drawable.flag_neth,R.drawable.flag_newzealand,R.drawable.flag_pak,R.drawable.flag_scot,R.drawable.flag_south_africa,R.drawable.flag_srilanka,R.drawable.flag_usa,R.drawable.flag_west_indies,R.drawable.flag_zimba};
	String[] allColors={"#00CC99","#00CC99","#FFFFFF","#000000","#000000","#B82500","#000000","#0099CC","#000000","#009933","#CF2900","#000000","#CF2900","#FFFFFF","#0099CC","#00FF00","#FFFFFF"};
	int pos;
	String score;
	boolean posStatus,soundStatus,stoppable,startable;
	
	String format;
	SharedPreferences prefHigh;
	Typeface customFont;
	MediaPlayer playerMove;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	//	requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.highscore_each_layout);
		

		txtScore=(TextView)findViewById(R.id.txtScore);
		imgScore=(ImageView)findViewById(R.id.imgScore);
		frameScore=(FrameLayout)findViewById(R.id.frameScore);
		
		customFont=Typeface.createFromAsset(this.getAssets(), "font/isocpeur.ttf");
		txtScore.setTypeface(customFont,Typeface.BOLD);

		startable=false;
		stoppable=true;

	//	TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
	//	String uid = tManager.getDeviceId();

		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.build();
		mAdView.loadAd(adRequest);
		
		format=getIntent().getExtras().getString("format");
		prefHigh=getSharedPreferences("cricket_score", 0);
		soundStatus=getIntent().getExtras().getBoolean("soundStatus");
		
		String high=prefHigh.getString(format, "-1/10_2");

		if(high.equals("-1/10_2"))
		{
			txtScore.setText("0/0");
			pos=2;
		}
		else
		{
			score="";
			pos=0;
			posStatus=false;
			for(int i=0;i<high.length();i++)
			{
				if(high.charAt(i)=='_') posStatus=true;
				else
				{
					if(posStatus) pos=pos*10+(high.charAt(i)-48);
					else score+=high.charAt(i);
				}
			}
			txtScore.setText(score);
			imgScore.setImageResource(flags[pos]);
		}
		txtScore.setTextColor(Color.parseColor(allColors[pos]));

		
	}



	@Override
	protected void onResume() {
		super.onResume();
		if(startable&&soundStatus)
		{
			MusicClass.startMusic();
			stoppable=true;
			startable=false;
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(stoppable&&soundStatus)
		{
			MusicClass.stopMusic();
			startable=true;
			stoppable=false;
		}
	}

	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent in=new Intent(HighScoreEach.this, HighScoreOptions.class);
		in.putExtra("soundStatus", soundStatus);
		stoppable=false;
		startActivity(in);
	    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
	    finish();
	}
	

}
