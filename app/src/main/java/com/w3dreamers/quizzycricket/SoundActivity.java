package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class SoundActivity extends Activity{
	
	LinearLayout linearSoundState,linearSoundStateText;
	TextView txtSoundState;
	
	boolean soundStatus,stoppable,startable;
	MediaPlayer playerMove;
	Typeface customFont;
	
	SharedPreferences prefSound;
	SharedPreferences.Editor soundEditor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Log.d("Status: ", "On create called");
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.sound_layout);
		linearSoundState=(LinearLayout)findViewById(R.id.linearSoundState);
		linearSoundStateText=(LinearLayout)findViewById(R.id.linearSoundStateText);
		txtSoundState=(TextView)findViewById(R.id.txtSoundState);
		
		customFont=Typeface.createFromAsset(this.getAssets(), "font/isocpeur.ttf");
		txtSoundState.setTypeface(customFont, Typeface.BOLD);

		startable=false;
		stoppable=true;

		Toast.makeText(getApplicationContext(),"Oncreate called",Toast.LENGTH_LONG).show();

		TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		String uid = tManager.getDeviceId();

		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(uid)
				.build();
		mAdView.loadAd(adRequest);

		if(savedInstanceState==null)
		{
			soundStatus=getIntent().getExtras().getBoolean("soundStatus");
		}
		else
		{
			soundStatus=savedInstanceState.getBoolean("soundStatus");
		}

		if(soundStatus)
		{
			linearSoundState.setBackgroundResource(R.drawable.sound_on);
			txtSoundState.setText("ON");
		}
		else
		{
			linearSoundState.setBackgroundResource(R.drawable.sound_off);
			txtSoundState.setText("OFF");
		}
		
		prefSound=getSharedPreferences("dadagiri_sound", 0);
		soundEditor=prefSound.edit();
		
		linearSoundState.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(soundStatus)
				{
					linearSoundState.setBackgroundResource(R.drawable.sound_off);
					txtSoundState.setText("OFF");
					soundStatus=false;
					MusicClass.stopMusic();
					startable=true;
					stoppable=false;
				}
				else
				{
					playerMove=MediaPlayer.create(SoundActivity.this, R.raw.move);
					playerMove.start();
					linearSoundState.setBackgroundResource(R.drawable.sound_on);
					txtSoundState.setText("ON");
					soundStatus=true;
					MusicClass.startMusic();
					startable=false;
					stoppable=true;
				}
				soundEditor.putBoolean("soundStatus", soundStatus);
				soundEditor.commit();
			}
		});
		
		
		linearSoundStateText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(soundStatus)
				{
					linearSoundState.setBackgroundResource(R.drawable.sound_off);
					txtSoundState.setText("OFF");
					soundStatus=false;
					MusicClass.stopMusic();
					startable=true;
					stoppable=false;
				}
				else
				{
					playerMove=MediaPlayer.create(SoundActivity.this, R.raw.move);
					playerMove.start();
					linearSoundState.setBackgroundResource(R.drawable.sound_on);
					txtSoundState.setText("ON");
					soundStatus=true;
					MusicClass.startMusic();
					startable=false;
					stoppable=true;
				}
				soundEditor.putBoolean("soundStatus", soundStatus);
				soundEditor.commit();
			}
		});
		
		
		
	}



	@Override
	protected void onResume() {
		super.onResume();
		if(startable&&soundStatus)
		{
			MusicClass.startMusic();
			stoppable=true;
			startable=false;
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(stoppable&&soundStatus)
		{
			MusicClass.stopMusic();
			startable=true;
			stoppable=false;
		}
	}
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent in=new Intent(SoundActivity.this,OptionActivity.class);
		in.putExtra("soundStatus", soundStatus);
		in.putExtra("startable", false);
		stoppable=false;
		startActivity(in);
	    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
	    finish();
	}


	@Override
	public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
		super.onSaveInstanceState(outState, outPersistentState);
		outState.putBoolean("soundStatus",soundStatus);
	}
}
