package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class OptionActivity extends Activity{
	
	LinearLayout linearStart,linearInstruction,linearHigh,linearSound,linearRate,linearFacebook;
	TextView txtStartGame,txtInstruction,txtHigh,txtSound;
	
	MediaPlayer playerMove;
	boolean clickable,soundStatus,stoppable,startable;
	
	Typeface customFont;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.option_layout);
		linearStart=(LinearLayout)findViewById(R.id.linearStartGame);
		linearInstruction=(LinearLayout)findViewById(R.id.linearInstructions);
		linearHigh=(LinearLayout)findViewById(R.id.linearHighScore);
		linearSound=(LinearLayout)findViewById(R.id.linearSound);
		linearRate=(LinearLayout)findViewById(R.id.linearRate);
		linearFacebook=(LinearLayout)findViewById(R.id.linearFacebook);
		txtStartGame=(TextView)findViewById(R.id.txtStartGame);
		txtInstruction=(TextView)findViewById(R.id.txtInstructions);
		txtHigh=(TextView)findViewById(R.id.txtHighScore);
		txtSound=(TextView)findViewById(R.id.txtSound);
		
		
		customFont=Typeface.createFromAsset(this.getAssets(), "font/isocpeur.ttf");
		txtStartGame.setTypeface(customFont,Typeface.BOLD);
		txtInstruction.setTypeface(customFont,Typeface.BOLD);
		txtHigh.setTypeface(customFont, Typeface.BOLD);
		txtSound.setTypeface(customFont, Typeface.BOLD);
		
		soundStatus=getIntent().getExtras().getBoolean("soundStatus");
		startable=getIntent().getExtras().getBoolean("startable");
		
		playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
		clickable=true;
		stoppable=true;


		TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		String uid = tManager.getDeviceId();

		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(uid)
				.build();
		mAdView.loadAd(adRequest);
		

		
		linearStart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					Intent in=new Intent(OptionActivity.this,CountryActivity.class);
					in.putExtra("soundStatus", soundStatus);
					in.putExtra("position", 2);
					stoppable=false;
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();
				}
			}
		});
		
		
		linearInstruction.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}	
					Intent in=new Intent(OptionActivity.this,InstructionActivity.class);
					in.putExtra("soundStatus", soundStatus);
					stoppable=false;
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();
				}
			}
		});

		
	    
	  /*  
		linearSettings.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
				//am.setStreamVolume(AudioManager.STREAM_MUSIC,am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),0);
				//am.setStreamVolume(AudioManager.STREAM_MUSIC,am.getMode(),0);
				am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_SAME, AudioManager.FLAG_SHOW_UI); 
				
			}
		});   */
	    
		linearHigh.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					showScoreOptions();
				}
			}
		});
			
		
		
		
		linearSound.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					Intent in=new Intent(OptionActivity.this, SoundActivity.class);
					in.putExtra("soundStatus", soundStatus);
					stoppable=false;
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();
				}
				
			}
		});



		linearRate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(clickable)
				{
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					Uri uri = Uri.parse("market://details?id=" + getPackageName());
					Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
					stoppable=true;
					try {
						startActivity(myAppLinkToMarket);
					} catch (ActivityNotFoundException e) {
						//Toast.makeText(getApplicationContext(), "Sorry. Rating of this application is not possible", Toast.LENGTH_LONG).show();
					}
				}
			}
		});


		linearFacebook.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(clickable)
				{
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					LayoutInflater li = LayoutInflater.from(OptionActivity.this);
					View promptsView = li.inflate(R.layout.score_type, null);
					AlertDialog.Builder builder = new AlertDialog.Builder(OptionActivity.this);
				//	builder.setView(promptsView);
					builder.setCancelable(true);
					final AlertDialog alertDialog = builder.create();
					alertDialog.setView(promptsView,0,0,0,0);
					alertDialog.show();

					LinearLayout linearGlobal=(LinearLayout)promptsView.findViewById(R.id.linearGlobal);
					LinearLayout linearLocal=(LinearLayout)promptsView.findViewById(R.id.linearLocal);
					TextView txtTitle=(TextView)promptsView.findViewById(R.id.txtTitle);
					TextView txtGlobal=(TextView)promptsView.findViewById(R.id.txtGlobal);
					TextView txtLocal=(TextView)promptsView.findViewById(R.id.txtLocal);

					txtTitle.setTypeface(customFont,Typeface.BOLD);
					txtGlobal.setTypeface(customFont,Typeface.BOLD);
					txtLocal.setTypeface(customFont,Typeface.BOLD);

					txtTitle.setText("Facebook");
					txtLocal.setText("Like and Feedback");
					txtGlobal.setText("Meet Developers");


					linearLocal.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if (clickable) {
								if (soundStatus) {
									playerMove = MediaPlayer.create(OptionActivity.this, R.raw.move);
									playerMove.start();
								}
								stoppable=true;
								startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/quizzycricket")));
							}
						}
					});

					linearGlobal.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(clickable)
							{
								if(soundStatus)
								{
									playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
									playerMove.start();
								}
								stoppable=true;
								startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/pages/W3-Dreamers/279123222268078")));
							}
						}
					});
				}
			}
		});
	    
		
		
	}


	@Override
	protected void onResume() {
		super.onResume();
		if(startable&&soundStatus)
		{
			MusicClass.startMusic();
			stoppable=true;
			startable=false;
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(stoppable&&soundStatus)
		{
			MusicClass.stopMusic();
			startable=true;
			stoppable=false;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		if(clickable)
		{
			LayoutInflater li = LayoutInflater.from(OptionActivity.this);
			View promptsView = li.inflate(R.layout.exit2, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			//builder.setView(promptsView);
			builder.setCancelable(true);
			final AlertDialog alertDialog = builder.create();
			alertDialog.setView(promptsView,0,0,0,0);
			alertDialog.show();
			
			LinearLayout linearDialogYes=(LinearLayout)promptsView.findViewById(R.id.linearDialogYes);
			LinearLayout linearDialogNo=(LinearLayout)promptsView.findViewById(R.id.linearDialogNo);
			TextView txtDialogBody=(TextView)promptsView.findViewById(R.id.txtDialogBody);
			TextView txtAlert=(TextView)promptsView.findViewById(R.id.txtAlert);
			TextView txtQuit=(TextView)promptsView.findViewById(R.id.txtQuit);
			TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);

			txtAlert.setTypeface(customFont,Typeface.BOLD);
			txtQuit.setTypeface(customFont,Typeface.BOLD);
			txtCancel.setTypeface(customFont,Typeface.BOLD);
			txtDialogBody.setTypeface(customFont);

			
			txtDialogBody.setText("Are you sure you want to quit right now?");
			txtAlert.setText("Alert");
			txtQuit.setText("Quit");
			txtCancel.setText("Cancel");
			
			linearDialogNo.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					alertDialog.cancel();
				}
			});	
			
			linearDialogYes.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	  
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					alertDialog.cancel();
					stoppable=true;
					System.exit(1);
				}
			});
			
			
		}
		
	}




	public boolean isMapAvailable()
	{
		int result= GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result== ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			//Toast.makeText(getApplicationContext(), "Google Play Service is not updated in your device", Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(getApplicationContext(),"Google Play Service is not supported in your device",Toast.LENGTH_LONG).show();
		}
		return false;
	}









	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		if(connectivity!=null)
		{
			NetworkInfo info=connectivity.getActiveNetworkInfo();
			if(info!=null&&info.isConnected()) return true;
		}
		return false;
	}



	public boolean isWifiEnabled()
	{
		WifiManager wifi=(WifiManager)getSystemService(Context.WIFI_SERVICE);
		return wifi.isWifiEnabled();
	}





	public void showScoreOptions()
	{
		LayoutInflater li = LayoutInflater.from(OptionActivity.this);
		View promptsView = li.inflate(R.layout.score_type, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(OptionActivity.this);
	//	builder.setView(promptsView);
		builder.setCancelable(true);
		final AlertDialog alertDialog = builder.create();
		alertDialog.setView(promptsView,0,0,0,0);
		alertDialog.show();
		clickable=true;

		LinearLayout linearGlobal=(LinearLayout)promptsView.findViewById(R.id.linearGlobal);
		LinearLayout linearLocal=(LinearLayout)promptsView.findViewById(R.id.linearLocal);
		TextView txtTitle=(TextView)promptsView.findViewById(R.id.txtTitle);
		TextView txtGlobal=(TextView)promptsView.findViewById(R.id.txtGlobal);
		TextView txtLocal=(TextView)promptsView.findViewById(R.id.txtLocal);

		txtTitle.setTypeface(customFont,Typeface.BOLD);
		txtGlobal.setTypeface(customFont,Typeface.BOLD);
		txtLocal.setTypeface(customFont,Typeface.BOLD);

		txtTitle.setText("Score Type");
		txtLocal.setText("Local Scores");
		txtGlobal.setText("Global Scores");


		linearLocal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (clickable) {
					clickable = false;
					if (soundStatus) {
						playerMove = MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					alertDialog.cancel();
					Intent in = new Intent(OptionActivity.this, HighScoreOptions.class);
					in.putExtra("soundStatus", soundStatus);
					stoppable=false;
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();
				}
			}
		});

		linearGlobal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					alertDialog.cancel();
					showGlobalScores();
				}
			}
		});
	}




	public void showGlobalScores()
	{
		if(isConnectingInternet()||isWifiEnabled())
		{
			if(isMapAvailable())
			{
				Intent in=new Intent(OptionActivity.this,GlobalScoreActivity.class);
				in.putExtra("soundStatus",soundStatus);
				in.putExtra("startable", false);
				stoppable=false;
				startActivity(in);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				finish();
			}
			else clickable=true;
		}
		else
		{
			LayoutInflater li = LayoutInflater.from(OptionActivity.this);
			View promptsView = li.inflate(R.layout.exit2, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(OptionActivity.this);
		//	builder.setView(promptsView);
			builder.setCancelable(true);
			final AlertDialog alertDialog = builder.create();
			alertDialog.setView(promptsView,0,0,0,0);
			alertDialog.show();
			clickable=true;

			LinearLayout linearDialogYes=(LinearLayout)promptsView.findViewById(R.id.linearDialogYes);
			LinearLayout linearDialogNo=(LinearLayout)promptsView.findViewById(R.id.linearDialogNo);
			TextView txtDialogBody=(TextView)promptsView.findViewById(R.id.txtDialogBody);
			TextView txtAlert=(TextView)promptsView.findViewById(R.id.txtAlert);
			TextView txtQuit=(TextView)promptsView.findViewById(R.id.txtQuit);
			TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);

			txtAlert.setTypeface(customFont,Typeface.BOLD);
			txtQuit.setTypeface(customFont,Typeface.BOLD);
			txtCancel.setTypeface(customFont,Typeface.BOLD);
			txtDialogBody.setTypeface(customFont);


			txtDialogBody.setText("Please turn ON internet connection or enable WIFI of your device");
			txtAlert.setText("Alert");
			txtQuit.setText("Quit");
			txtCancel.setText("Cancel");

			linearDialogNo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					alertDialog.cancel();
				}
			});

			linearDialogYes.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(OptionActivity.this, R.raw.move);
						playerMove.start();
					}
					alertDialog.cancel();
					Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
					stoppable=true;
					startActivity(in);
				}
			});
		}
	}



	
	

}
