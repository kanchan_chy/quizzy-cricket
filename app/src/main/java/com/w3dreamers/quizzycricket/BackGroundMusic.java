package com.w3dreamers.quizzycricket;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

public class BackGroundMusic extends Service{
	
	MediaPlayer player;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		//super.onStartCommand(intent, flags, startId);
		player.setLooping(true);
		player.start();
		//Toast.makeText(getApplicationContext(), "onStart", Toast.LENGTH_LONG).show();
		return START_STICKY;
	}
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		//Toast.makeText(getApplicationContext(), "onCreate", Toast.LENGTH_LONG).show();
		player=MediaPlayer.create(getApplicationContext(), R.raw.music);
		super.onCreate();
		
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		//Toast.makeText(getApplicationContext(), "onDestroy", Toast.LENGTH_LONG).show();
		player.pause();
		super.onDestroy();
	}

}
