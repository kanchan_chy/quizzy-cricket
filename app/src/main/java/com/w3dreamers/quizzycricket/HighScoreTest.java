package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class HighScoreTest extends Activity{
	
	ImageView imgScore1,imgScore2;
	TextView txtScore1,txtScore2;
	
	int[] flags={R.drawable.flag_afg,R.drawable.flag_austr,R.drawable.flag_bang,R.drawable.flag_canada,R.drawable.flag_england,R.drawable.flag_india,R.drawable.flag_ireland,R.drawable.flag_kenya,R.drawable.flag_neth,R.drawable.flag_newzealand,R.drawable.flag_pak,R.drawable.flag_scot,R.drawable.flag_south_africa,R.drawable.flag_srilanka,R.drawable.flag_usa,R.drawable.flag_west_indies,R.drawable.flag_zimba};
	String[] allColors={"#00CC99","#00CC99","#FFFFFF","#000000","#000000","#B82500","#000000","#0099CC","#000000","#009933","#CF2900","#000000","#CF2900","#FFFFFF","#0099CC","#00FF00","#FFFFFF"};
	int pos;
	String score;
	boolean posStatus,soundStatus,stoppable,startable;

	SharedPreferences prefHigh;
	Typeface customFont;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.highscore_test_layout);

		imgScore1=(ImageView)findViewById(R.id.imgScore1);
		imgScore2=(ImageView)findViewById(R.id.imgScore2);
		txtScore1=(TextView)findViewById(R.id.txtScore1);
		txtScore2=(TextView)findViewById(R.id.txtScore2);
		
		customFont=Typeface.createFromAsset(this.getAssets(), "font/isocpeur.ttf");
		txtScore1.setTypeface(customFont,Typeface.BOLD);
		txtScore2.setTypeface(customFont,Typeface.BOLD);

		startable=false;
		stoppable=true;

	//	TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
	//	String uid = tManager.getDeviceId();

		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.build();
		mAdView.loadAd(adRequest);

		prefHigh=getSharedPreferences("cricket_score", 0);
		soundStatus=getIntent().getExtras().getBoolean("soundStatus");
		
		String high1=prefHigh.getString("test_1", "-1/10_2");
		String high2=prefHigh.getString("test_2", "-1/10_2");
		
		if(high1.equals("-1/10_2"))
		{
			txtScore1.setText("Innings 1\n0/0");
			pos=2;
		}
		else
		{
			score="";
			pos=0;
			posStatus=false;
			for(int i=0;i<high1.length();i++)
			{
				if(high1.charAt(i)=='_') posStatus=true;
				else
				{
					if(posStatus) pos=pos*10+(high1.charAt(i)-48);
					else score+=high1.charAt(i);
				}
			}
			txtScore1.setText("Innings 1\n" + score);
			imgScore1.setImageResource(flags[pos]);
		}
		txtScore1.setTextColor(Color.parseColor(allColors[pos]));
		
		if(high2.equals("-1/10_2"))
		{
			txtScore2.setText("Innings 2\n0/0");
			pos=2;
		}
		else
		{
			score="";
			pos=0;
			posStatus=false;
			for(int i=0;i<high2.length();i++)
			{
				if(high2.charAt(i)=='_') posStatus=true;
				else
				{
					if(posStatus) pos=pos*10+(high2.charAt(i)-48);
					else score+=high2.charAt(i);
				}
			}
			txtScore2.setText("Innings 2\n" + score);
			imgScore2.setImageResource(flags[pos]);
		}
		txtScore2.setTextColor(Color.parseColor(allColors[pos]));

		
	}




	@Override
	protected void onResume() {
		super.onResume();
		if(startable&&soundStatus)
		{
			MusicClass.startMusic();
			stoppable=true;
			startable=false;
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(stoppable&&soundStatus)
		{
			MusicClass.stopMusic();
			startable=true;
			stoppable=false;
		}
	}


	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent in=new Intent(HighScoreTest.this,HighScoreOptions.class);
		in.putExtra("soundStatus", soundStatus);
		stoppable=false;
		startActivity(in);
	    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
	    finish();
	}
	

}
