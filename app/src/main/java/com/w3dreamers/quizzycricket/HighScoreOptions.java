package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class HighScoreOptions extends Activity{
	
	LinearLayout linearT20,linearODI,linearTest;
	TextView txtT20,txtODI,txtTest;
	
	MediaPlayer playerMove;
	boolean clickable,soundStatus,stoppable,startable;
	Typeface customFont;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.highscore_option_layout);
		
		linearT20=(LinearLayout)findViewById(R.id.linearT20);
		linearODI=(LinearLayout)findViewById(R.id.linearODI);
		linearTest=(LinearLayout)findViewById(R.id.linearTest);
		txtT20=(TextView)findViewById(R.id.txtT20);
	    txtODI=(TextView)findViewById(R.id.txtODI);
	    txtTest=(TextView)findViewById(R.id.txtTest);
		
		customFont=Typeface.createFromAsset(this.getAssets(), "font/isocpeur.ttf");
		txtT20.setTypeface(customFont, Typeface.BOLD);
		txtODI.setTypeface(customFont, Typeface.BOLD);
		txtTest.setTypeface(customFont, Typeface.BOLD);

		startable=false;
		stoppable=true;

	//	TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
	//	String uid = tManager.getDeviceId();

		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.build();
		mAdView.loadAd(adRequest);
		
		soundStatus=getIntent().getExtras().getBoolean("soundStatus");
		
		playerMove=MediaPlayer.create(this, R.raw.move);
		clickable=true;
		
		
		linearT20.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus) playerMove.start();
					Intent in=new Intent(HighScoreOptions.this,HighScoreEach.class);
					in.putExtra("format", "t20");
					in.putExtra("soundStatus", soundStatus);
					stoppable=false;
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				    finish();
				}
			}
		});
		
		
		linearODI.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus) playerMove.start();
					Intent in=new Intent(HighScoreOptions.this,HighScoreEach.class);
					in.putExtra("format", "odi");
					in.putExtra("soundStatus", soundStatus);
					stoppable=false;
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				    finish();
				}
			}
		});
		
		
		linearTest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus) playerMove.start();
					Intent in=new Intent(HighScoreOptions.this, HighScoreTest.class);
					in.putExtra("soundStatus", soundStatus);
					stoppable=false;
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				    finish();
				}
			}
		});
		
		
		
	}



	@Override
	protected void onResume() {
		super.onResume();
		if(startable&&soundStatus)
		{
			MusicClass.startMusic();
			stoppable=true;
			startable=false;
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(stoppable&&soundStatus)
		{
			MusicClass.stopMusic();
			startable=true;
			stoppable=false;
		}
	}

	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(clickable)
		{
			clickable=false;
			Intent in=new Intent(HighScoreOptions.this,OptionActivity.class);
			in.putExtra("soundStatus", soundStatus);
			in.putExtra("startable", false);
			stoppable=false;
			startActivity(in);
		    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
		    finish();
		}
	}
	

}
