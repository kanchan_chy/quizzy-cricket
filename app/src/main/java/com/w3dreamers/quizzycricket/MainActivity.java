package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

public class MainActivity extends Activity {
	
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	boolean notUpdated=false;
	String value="";
	
	SharedPreferences prefSound;
	boolean soundStatus;

	MusicClass music;
	
	DbHelper dbOpenHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main2);

		music=new MusicClass(MainActivity.this);
		
		prefSound=getSharedPreferences("dadagiri_sound", 0);
		soundStatus=prefSound.getBoolean("soundStatus", true);
		
		pref=getSharedPreferences("database_update_dadagiri_cricket",0);
		value=pref.getString("load", "no");
		if(value.equals("no"))
		{
			notUpdated=true;
		}
		else 
			notUpdated=false;
		
		AsyncTaskRunner runner = new AsyncTaskRunner();
		runner.execute();
		
		
	}
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}
	
	
	
	public class	AsyncTaskRunner extends AsyncTask<String, String, String>
	{		
		
	@Override
	protected String doInBackground(String... arg0) {
		// TODO Auto-generated method stub
		try
		{
			if(notUpdated)
			{							
				dbOpenHelper = new DbHelper(MainActivity.this, Constants.DATABASE_NAME);
				editor=pref.edit();
				editor.putString("load","yes");			
				editor.commit();
			}
			Thread.sleep(3000);			
		}
			
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
	}
	
	protected void onPostExecute(String string)
	{
		    Intent in=new Intent(MainActivity.this, OptionActivity.class);
		    in.putExtra("soundStatus", soundStatus);
		    in.putExtra("startable", true);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);	
			finish();		
	}
	
	
	}
	
	

}
