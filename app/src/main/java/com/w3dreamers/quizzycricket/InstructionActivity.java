package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

/*
import com.easyandroidanimations.library.Animation;
import com.easyandroidanimations.library.AnimationListener;
import com.easyandroidanimations.library.SlideInAnimation;   */

public class InstructionActivity extends Activity implements Animation.AnimationListener{
	
	LinearLayout linearNext;
	TextView txtInstruction,txtNext;
	
	ArrayList<String> instructionSet;
	int insPosition;
	
	boolean clickable,soundStatus,stoppable,startable;
	MediaPlayer playerMove;
	Typeface customFont;

	Animation toLeft,fromRight;


	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.layout_instruction);
		
		linearNext=(LinearLayout)findViewById(R.id.linearNext);
		txtInstruction=(TextView)findViewById(R.id.txtInstruction);
		txtNext=(TextView)findViewById(R.id.txtNext);
		
		customFont=Typeface.createFromAsset(this.getAssets(), "font/isocpeur.ttf");
		txtInstruction.setTypeface(customFont);
		txtNext.setTypeface(customFont, Typeface.BOLD);
		
		instructionSet=new ArrayList<String>();

		startable=false;
		stoppable=true;

		toLeft= AnimationUtils.loadAnimation(this, R.anim.swipe_to_left);
		fromRight= AnimationUtils.loadAnimation(this,R.anim.swipe_from_right);

		toLeft.setAnimationListener(this);
		fromRight.setAnimationListener(this);



		TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		String uid = tManager.getDeviceId();

		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(uid)
				.build();
		mAdView.loadAd(adRequest);

		
		instructionSet.add("This is an interesting Game Show");
		instructionSet.add("The game can be played in all cricket formats: T20, ODI and Test");
		instructionSet.add("Sound can be controlled in sound option");
		instructionSet.add("Here each over consists of only ball");
		instructionSet.add("Player has to choose run before playing a ball");
		instructionSet.add("Complexity of question depends on runs choosen");
		instructionSet.add("Each wrong answer will result in a wicket");
		instructionSet.add("Answer should be given within 30 seconds");
		instructionSet.add("Over time will result in a wicket");
		instructionSet.add("Player can make a ball dot by pressing dot option");
		instructionSet.add("Game will be over if 10 wickets are gone before ending all overs");
		instructionSet.add("In T20 maximum 20 overs can be played");
		instructionSet.add("In ODI maximum 50 overs can be played");
		instructionSet.add("In Test maximum 2 innings can be played within fixed time period");
		instructionSet.add("Scores all over the world can be viewed through Leaderboards");
		instructionSet.add("Achievements will be provided depending on performances");
		instructionSet.add("Enjoy the game");

		if(savedInstanceState==null)
		{
			insPosition=1;
		}
		else
		{
			insPosition=savedInstanceState.getInt("pos");
		}
		if(insPosition==0)
		{
			txtInstruction.setText(instructionSet.get(instructionSet.size()-1));
			txtNext.setText("From Beginning");
		}
		else
		{
			txtInstruction.setText(instructionSet.get(insPosition-1));
			txtNext.setText("Next");
		}
		
		soundStatus=getIntent().getExtras().getBoolean("soundStatus");
		
		clickable=true;
		
		
		
		
		linearNext.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					clickable=false;
					if(soundStatus)
					{
						playerMove=MediaPlayer.create(InstructionActivity.this, R.raw.move);
						playerMove.start();
					}

					txtInstruction.startAnimation(toLeft);
					
			/*		new SlideInAnimation(txtInstruction).setInterpolator(new DecelerateInterpolator())
					.setListener(new AnimationListener() {
						
						@Override
						public void onAnimationEnd(Animation arg0) {
							// TODO Auto-generated method stub
							//txtAnimate.setVisibility(View.VISIBLE);
							clickable=true;
						}
					}).animate();   */
					
					
				}
			}
		});
		
	}




	@Override
	protected void onResume() {
		super.onResume();
		if(startable&&soundStatus)
		{
			MusicClass.startMusic();
			stoppable=true;
			startable=false;
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(stoppable&&soundStatus)
		{
			MusicClass.stopMusic();
			startable=true;
			stoppable=false;
		}
	}


	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(clickable)
		{
			clickable=false;
			Intent in=new Intent(InstructionActivity.this, OptionActivity.class);
			in.putExtra("soundStatus", soundStatus);
			in.putExtra("startable", false);
			stoppable=false;
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
			finish();
		}
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("pos",insPosition);
	}

	@Override
	public void onAnimationStart(Animation animation) {

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		if(animation==toLeft)
		{
			txtInstruction.setText(instructionSet.get(insPosition));
			insPosition++;
			if(insPosition==instructionSet.size())
			{
				txtNext.setText("From Beginning");
				insPosition=0;
			}
			else if(insPosition==1) txtNext.setText("Next");

			txtInstruction.startAnimation(fromRight);
		}
		else clickable=true;
	}

	@Override
	public void onAnimationRepeat(Animation animation) {

	}
}
