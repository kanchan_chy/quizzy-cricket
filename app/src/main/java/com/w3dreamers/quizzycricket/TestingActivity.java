package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASUS on 9/12/2015.
 */
public class TestingActivity extends Activity {

    TextView txt;
    ArrayList<String>questions=new ArrayList<String>();
    ArrayList<String>optionAs=new ArrayList<String>();
    ArrayList<String>optionBs=new ArrayList<String>();
    ArrayList<String>optionCs=new ArrayList<String>();
    ArrayList<String>optionDs=new ArrayList<String>();
    ArrayList<String>answers=new ArrayList<String>();
    ArrayList<String>ids=new ArrayList<String>();

    DbHelper dbOpenHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testing_layout);
        txt=(TextView)findViewById(R.id.txt);


        try
        {
            dbOpenHelper = new DbHelper(TestingActivity.this, Constants.DATABASE_NAME,1);
        }
        catch(Exception e)
        {}


        try
        {
            ArrayList<String>columnName=new ArrayList<String>();
            columnName.add(Constants.LEVEL_5_QUESTION);
            columnName.add(Constants.LEVEL_5_OPTION_A);
            columnName.add(Constants.LEVEL_5_OPTION_B);
            columnName.add(Constants.LEVEL_5_OPTION_C);
            columnName.add(Constants.LEVEL_5_OPTION_D);
            columnName.add(Constants.LEVEL_5_ANSWER);
            columnName.add(Constants.LEVEL_5_ID);

            HashMap<String,ArrayList<String>> data=dbOpenHelper.getAllRowByColumn(Constants.TABLE_LEVEL_5,columnName,Constants.LEVEL_5_ID);
            if(data.get(Constants.LEVEL_5_QUESTION).size()>0) questions=data.get(Constants.LEVEL_5_QUESTION);
            if(data.get(Constants.LEVEL_5_OPTION_A).size()>0) optionAs=data.get(Constants.LEVEL_5_OPTION_A);
            if(data.get(Constants.LEVEL_5_OPTION_B).size()>0) optionBs=data.get(Constants.LEVEL_5_OPTION_B);
            if(data.get(Constants.LEVEL_5_OPTION_C).size()>0) optionCs=data.get(Constants.LEVEL_5_OPTION_C);
            if(data.get(Constants.LEVEL_5_OPTION_D).size()>0) optionDs=data.get(Constants.LEVEL_5_OPTION_D);
            if(data.get(Constants.LEVEL_5_ANSWER).size()>0) answers=data.get(Constants.LEVEL_5_ANSWER);
            if(data.get(Constants.LEVEL_5_ID).size()>0) ids=data.get(Constants.LEVEL_5_ID);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Error in retreiving data",Toast.LENGTH_LONG).show();
        }

        String text="";
        for(int i=0;i<ids.size();i++)
        {
            String question,optionA,optionB,optionC,optionD,answer,id;
            question=questions.get(i);
            optionA=optionAs.get(i);
            optionB=optionBs.get(i);
            optionC=optionCs.get(i);
            optionD=optionDs.get(i);
            answer=answers.get(i);
            id=ids.get(i);

            if(!answer.equals(optionA)&&!answer.equals(optionB)&&!answer.equals(optionC)&&!answer.equals(optionD))
                text+=id+"\n";
        }

        txt.setText(text);


    }
}
