package com.w3dreamers.quizzycricket;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class CricFormatActivity extends Activity{
	
	ImageView imgCountry;
	LinearLayout linearT20,linearODI,linearTest;
	TextView txtT20,txtODI,txtTest;
	
	MediaPlayer playerMove;
	boolean clickable,soundStatus,supported,stoppable,startable;
	
	int[] flags={R.drawable.flag_afg,R.drawable.flag_austr,R.drawable.flag_bang,R.drawable.flag_canada,R.drawable.flag_england,R.drawable.flag_india,R.drawable.flag_ireland,R.drawable.flag_kenya,R.drawable.flag_neth,R.drawable.flag_newzealand,R.drawable.flag_pak,R.drawable.flag_scot,R.drawable.flag_south_africa,R.drawable.flag_srilanka,R.drawable.flag_usa,R.drawable.flag_west_indies,R.drawable.flag_zimba};
	int position,result;
	
	Typeface customFont;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.cric_format_layout);
		imgCountry=(ImageView)findViewById(R.id.imgCountry);
	    linearT20=(LinearLayout)findViewById(R.id.linearT20);
		linearODI=(LinearLayout)findViewById(R.id.linearODI);
	    linearTest=(LinearLayout)findViewById(R.id.linearTest);
	    txtT20=(TextView)findViewById(R.id.txtT20);
	    txtODI=(TextView)findViewById(R.id.txtODI);
	    txtTest=(TextView)findViewById(R.id.txtTest);
		
		position=getIntent().getExtras().getInt("position");
		soundStatus=getIntent().getExtras().getBoolean("soundStatus");
		
		customFont=Typeface.createFromAsset(this.getAssets(), "font/isocpeur.ttf");
		txtT20.setTypeface(customFont, Typeface.BOLD);
		txtODI.setTypeface(customFont, Typeface.BOLD);
		txtTest.setTypeface(customFont, Typeface.BOLD);
		
		imgCountry.setImageResource(flags[position]);
		
		playerMove=MediaPlayer.create(this, R.raw.move);
		clickable=true;
		supported=false;
		startable=false;
		stoppable=true;


	//	TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
	//	String uid = tManager.getDeviceId();

		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.build();
		mAdView.loadAd(adRequest);
		
		
		linearT20.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					//clickable=false;
					if(soundStatus) playerMove.start();
					startGame("t20");
				}	    
			}
		});
		
		linearODI.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					//clickable=false;
					if(soundStatus) playerMove.start();
					startGame("odi");
				}
			}
		});
		
		linearTest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(clickable)
				{
					//clickable=false;
					if(soundStatus) playerMove.start();
					startGame("test");
				}
			}
		});
		
		
	}





	@Override
	protected void onResume() {
		super.onResume();
		if(startable&&soundStatus)
		{
			MusicClass.startMusic();
			stoppable=true;
			startable=false;
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(stoppable&&soundStatus)
		{
			MusicClass.stopMusic();
			startable=true;
			stoppable=false;
		}
	}



	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(clickable)
		{
			clickable=false;
			Intent in=new Intent(CricFormatActivity.this,CountryActivity.class);
			in.putExtra("position", position);
			in.putExtra("soundStatus", soundStatus);
			stoppable=false;
			startActivity(in);
		    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
		    finish();
		}
	}




	public boolean isMapAvailable()
	{
		result= GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result== ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			supported=true;
		/*	Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();  */
		}
		else
		{
			supported=false;
			//Toast.makeText(getApplicationContext(), "Google Play Service is not supported in your device", Toast.LENGTH_LONG).show();
		}
		return false;
	}









	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		if(connectivity!=null)
		{
			NetworkInfo info=connectivity.getActiveNetworkInfo();
			if(info!=null&&info.isConnected()) return true;
		}
		return false;
	}



	public boolean isWifiEnabled()
	{
		WifiManager wifi=(WifiManager)getSystemService(Context.WIFI_SERVICE);
		return wifi.isWifiEnabled();
	}




	public void startGame(final String format)
	{
		if(isConnectingInternet()||isWifiEnabled())
		{
			if(isMapAvailable())
			{
				Intent in=new Intent(CricFormatActivity.this, GameActivity.class);
				in.putExtra("position", position);
				in.putExtra("format", format);
				in.putExtra("soundStatus", soundStatus);
				startActivity(in);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				finish();
			}
			else
			{
				if(!supported)
				{
					Intent in=new Intent(CricFormatActivity.this, GameActivity.class);
					in.putExtra("position", position);
					in.putExtra("format", format);
					in.putExtra("soundStatus", soundStatus);
					stoppable=true;
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();
				}
				else
				{
					LayoutInflater li = LayoutInflater.from(CricFormatActivity.this);
					View promptsView = li.inflate(R.layout.exit2, null);
					AlertDialog.Builder builder = new AlertDialog.Builder(CricFormatActivity.this);
				//	builder.setView(promptsView);
					builder.setCancelable(true);
					final AlertDialog alertDialog = builder.create();
					alertDialog.setView(promptsView,0,0,0,0);
					alertDialog.show();
					clickable=true;

					LinearLayout linearDialogYes=(LinearLayout)promptsView.findViewById(R.id.linearDialogYes);
					LinearLayout linearDialogNo=(LinearLayout)promptsView.findViewById(R.id.linearDialogNo);
					TextView txtDialogBody=(TextView)promptsView.findViewById(R.id.txtDialogBody);
					TextView txtAlert=(TextView)promptsView.findViewById(R.id.txtAlert);
					TextView txtQuit=(TextView)promptsView.findViewById(R.id.txtQuit);
					TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);

					txtAlert.setTypeface(customFont,Typeface.BOLD);
					txtQuit.setTypeface(customFont,Typeface.BOLD);
					txtCancel.setTypeface(customFont,Typeface.BOLD);
					txtDialogBody.setTypeface(customFont);


					txtDialogBody.setText("Google Play Service is not updated in your device. Do you want to update it?");
					txtAlert.setText("Alert");
					txtQuit.setText("Update");
					txtCancel.setText("Cancel");

					linearDialogNo.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(soundStatus)
							{
								playerMove=MediaPlayer.create(CricFormatActivity.this, R.raw.move);
								playerMove.start();
							}
							alertDialog.cancel();
							Intent in=new Intent(CricFormatActivity.this, GameActivity.class);
							in.putExtra("position", position);
							in.putExtra("format", format);
							in.putExtra("soundStatus", soundStatus);
							stoppable=true;
							startActivity(in);
							overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
							finish();
						}
					});

					linearDialogYes.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(soundStatus)
							{
								playerMove=MediaPlayer.create(CricFormatActivity.this, R.raw.move);
								playerMove.start();
							}
							alertDialog.cancel();
							Dialog d=GooglePlayServicesUtil.getErrorDialog(result,CricFormatActivity.this,1);
							d.show();
						}
					});
				}
			}
		}
		else
		{
			Intent in=new Intent(CricFormatActivity.this, GameActivity.class);
			in.putExtra("position", position);
			in.putExtra("format", format);
			in.putExtra("soundStatus", soundStatus);
			stoppable=true;
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			finish();
		}
	}


	
	

}
