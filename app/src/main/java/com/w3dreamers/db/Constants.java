package com.w3dreamers.db;

import java.util.ArrayList;

public class Constants {
//-------------------------------------------------------------	
    public static final int DATABASE_VERSION = 1;
    
    public static final int STRING_ONLY=1;
    public static final int STRING_ANY=2;
    public static final int STRING_FIRST=3;
    public static final int STRING_LAST=4;
    
    public static final String IS_EQUAL="=";
    public static final String IS_NOT_EQUAL="!=";
    public static final String IS_GREATER=">";
    public static final String IS_LESS="<";
    public static final String IS_GREATER_EQUAL=">=";
    public static final String IS_LESS_EQUAL="<=";
    
    public static final int IS_NUMBER=1;
    public static final int IS_STRING=2;
    
    //-----------------------------------------------------------
    
    
    // Database Name
    public static final String DATABASE_NAME = "cricket_dadagiri.db";
 
    // Contacts table name
    public static final String TABLE_LEVEL_1 = "level_1"; 
    public static final String LEVEL_1_ID="id";
    public static final String LEVEL_1_QUESTION="question";
    public static final String LEVEL_1_OPTION_A="option_a";
    public static final String LEVEL_1_OPTION_B="option_b";
    public static final String LEVEL_1_OPTION_C="option_c";
    public static final String LEVEL_1_OPTION_D="option_d";
    public static final String LEVEL_1_ANSWER="answer";
    
    
    public static final String TABLE_LEVEL_2 = "level_2"; 
    public static final String LEVEL_2_ID="id";
    public static final String LEVEL_2_QUESTION="question";
    public static final String LEVEL_2_OPTION_A="option_a";
    public static final String LEVEL_2_OPTION_B="option_b";
    public static final String LEVEL_2_OPTION_C="option_c";
    public static final String LEVEL_2_OPTION_D="option_d";
    public static final String LEVEL_2_ANSWER="answer";
    
    public static final String TABLE_LEVEL_3 = "level_3"; 
    public static final String LEVEL_3_ID="id";
    public static final String LEVEL_3_QUESTION="question";
    public static final String LEVEL_3_OPTION_A="option_a";
    public static final String LEVEL_3_OPTION_B="option_b";
    public static final String LEVEL_3_OPTION_C="option_c";
    public static final String LEVEL_3_OPTION_D="option_d";
    public static final String LEVEL_3_ANSWER="answer";
    
    public static final String TABLE_LEVEL_4 = "level_4"; 
    public static final String LEVEL_4_ID="id";
    public static final String LEVEL_4_QUESTION="question";
    public static final String LEVEL_4_OPTION_A="option_a";
    public static final String LEVEL_4_OPTION_B="option_b";
    public static final String LEVEL_4_OPTION_C="option_c";
    public static final String LEVEL_4_OPTION_D="option_d";
    public static final String LEVEL_4_ANSWER="answer";
    
    
    public static final String TABLE_LEVEL_5 = "level_5"; 
    public static final String LEVEL_5_ID="id";
    public static final String LEVEL_5_QUESTION="question";
    public static final String LEVEL_5_OPTION_A="option_a";
    public static final String LEVEL_5_OPTION_B="option_b";
    public static final String LEVEL_5_OPTION_C="option_c";
    public static final String LEVEL_5_OPTION_D="option_d";
    public static final String LEVEL_5_ANSWER="answer";
    
    
    public ArrayList<String> getColumnName(String tableName)
    {
    	ArrayList<String> allColumn=new ArrayList<String>();
    	allColumn.clear();
    	if(tableName.equals(this.TABLE_LEVEL_1))
    	{
    		allColumn.add(LEVEL_1_ID);
    		allColumn.add(LEVEL_1_QUESTION);
    		allColumn.add(LEVEL_1_OPTION_A);
    		allColumn.add(LEVEL_1_OPTION_B);
    		allColumn.add(LEVEL_1_OPTION_C);
    		allColumn.add(LEVEL_1_OPTION_D);
    		allColumn.add(LEVEL_1_ANSWER);
    	}
    	else if(tableName.equals(this.TABLE_LEVEL_2))
    	{
    		allColumn.add(LEVEL_2_ID);
    		allColumn.add(LEVEL_2_QUESTION);
    		allColumn.add(LEVEL_2_OPTION_A);
    		allColumn.add(LEVEL_2_OPTION_B);
    		allColumn.add(LEVEL_2_OPTION_C);
    		allColumn.add(LEVEL_2_OPTION_D);
    		allColumn.add(LEVEL_2_ANSWER);
    	}
    	else if(tableName.equals(this.TABLE_LEVEL_3))
    	{
    		allColumn.add(LEVEL_3_ID);
    		allColumn.add(LEVEL_3_QUESTION);
    		allColumn.add(LEVEL_3_OPTION_A);
    		allColumn.add(LEVEL_3_OPTION_B);
    		allColumn.add(LEVEL_3_OPTION_C);
    		allColumn.add(LEVEL_3_OPTION_D);
    		allColumn.add(LEVEL_3_ANSWER);
    	}
    	else if(tableName.equals(this.TABLE_LEVEL_4))
    	{
    		allColumn.add(LEVEL_4_ID);
    		allColumn.add(LEVEL_4_QUESTION);
    		allColumn.add(LEVEL_4_OPTION_A);
    		allColumn.add(LEVEL_4_OPTION_B);
    		allColumn.add(LEVEL_4_OPTION_C);
    		allColumn.add(LEVEL_4_OPTION_D);
    		allColumn.add(LEVEL_4_ANSWER);
    	}
    	else if(tableName.equals(this.TABLE_LEVEL_5))
    	{
    		allColumn.add(LEVEL_5_ID);
    		allColumn.add(LEVEL_5_QUESTION);
    		allColumn.add(LEVEL_5_OPTION_A);
    		allColumn.add(LEVEL_5_OPTION_B);
    		allColumn.add(LEVEL_5_OPTION_C);
    		allColumn.add(LEVEL_5_OPTION_D);
    		allColumn.add(LEVEL_5_ANSWER);
    	}
   	
    	return allColumn;
    }

}
